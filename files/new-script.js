!function ($) {

    "use strict";

    // TABCOLLAPSE CLASS DEFINITION
    // ======================

    var TabCollapse = function (el, options) {
        this.options   = options;
        this.$tabs  = $(el);

        this._accordionVisible = false; //content is attached to tabs at first
        this._initAccordion();
        this._checkStateOnResize();


        // checkState() has gone to setTimeout for making it possible to attach listeners to
        // shown-accordion.bs.tabcollapse event on page load.
        // See https://github.com/flatlogic/bootstrap-tabcollapse/issues/23
        var that = this;
        setTimeout(function() {
            that.checkState();
        }, 0);
    };

    TabCollapse.DEFAULTS = {
        accordionClass: 'visible-xs',
        tabsClass: 'hidden-xs',
        accordionTemplate: function(heading, groupId, parentId, active) {
            return  '<div class="panel panel-default">' +
                '   <div class="panel-heading">' +
                '      <h4 class="panel-title">' +
                '      </h4>' +
                '   </div>' +
                '   <div id="' + groupId + '" class="panel-collapse collapse ' + (active ? 'in' : '') + '">' +
                '       <div class="panel-body js-tabcollapse-panel-body">' +
                '       </div>' +
                '   </div>' +
                '</div>'

        }
    };

    TabCollapse.prototype.checkState = function(){
        if (this.$tabs.is(':visible') && this._accordionVisible){
            this.showTabs();
            this._accordionVisible = false;
        } else if (this.$accordion.is(':visible') && !this._accordionVisible){
            this.showAccordion();
            this._accordionVisible = true;
        }
    };

    TabCollapse.prototype.showTabs = function(){
        var view = this;
        this.$tabs.trigger($.Event('show-tabs.bs.tabcollapse'));

        var $panelHeadings = this.$accordion.find('.js-tabcollapse-panel-heading').detach();

        $panelHeadings.each(function() {
            var $panelHeading = $(this),
                $parentLi = $panelHeading.data('bs.tabcollapse.parentLi');

            var $oldHeading = view._panelHeadingToTabHeading($panelHeading);

            $parentLi.removeClass('active');
            if ($parentLi.parent().hasClass('dropdown-menu') && !$parentLi.siblings('li').hasClass('active')) {
                $parentLi.parent().parent().removeClass('active');
            }

            if (!$oldHeading.hasClass('collapsed')) {
                $parentLi.addClass('active');
                if ($parentLi.parent().hasClass('dropdown-menu')) {
                    $parentLi.parent().parent().addClass('active');
                }
            } else {
                $oldHeading.removeClass('collapsed');
            }

            $parentLi.append($panelHeading);
        });

        if (!$('li').hasClass('active')) {
            $('li').first().addClass('active')
        }

        var $panelBodies = this.$accordion.find('.js-tabcollapse-panel-body');
        $panelBodies.each(function(){
            var $panelBody = $(this),
                $tabPane = $panelBody.data('bs.tabcollapse.tabpane');
            $tabPane.append($panelBody.contents().detach());
        });
        this.$accordion.html('');

        if(this.options.updateLinks) {
            var $tabContents = this.getTabContentElement();
            $tabContents.find('[data-toggle-was="tab"], [data-toggle-was="pill"]').each(function() {
                var $el = $(this);
                var href = $el.attr('href').replace(/-collapse$/g, '');
                $el.attr({
                    'data-toggle': $el.attr('data-toggle-was'),
                    'data-toggle-was': '',
                    'data-parent': '',
                    href: href
                });
            });
        }

        this.$tabs.trigger($.Event('shown-tabs.bs.tabcollapse'));
    };

    TabCollapse.prototype.getTabContentElement = function(){
        var $tabContents = $(this.options.tabContentSelector);
        if($tabContents.length === 0) {
            $tabContents = this.$tabs.siblings('.tab-content');
        }
        return $tabContents;
    };

    TabCollapse.prototype.showAccordion = function(){
        this.$tabs.trigger($.Event('show-accordion.bs.tabcollapse'));

        var $headings = this.$tabs.find('li:not(.dropdown) [data-toggle="tab"], li:not(.dropdown) [data-toggle="pill"]'),
            view = this;
        $headings.each(function(){
            var $heading = $(this),
                $parentLi = $heading.parent();
            $heading.data('bs.tabcollapse.parentLi', $parentLi);
            view.$accordion.append(view._createAccordionGroup(view.$accordion.attr('id'), $heading.detach()));
        });

        if(this.options.updateLinks) {
            var parentId = this.$accordion.attr('id');
            var $selector = this.$accordion.find('.js-tabcollapse-panel-body');
            $selector.find('[data-toggle="tab"], [data-toggle="pill"]').each(function() {
                var $el = $(this);
                var href = $el.attr('href') + '-collapse';
                $el.attr({
                    'data-toggle-was': $el.attr('data-toggle'),
                    'data-toggle': 'collapse',
                    'data-parent': '#' + parentId,
                    href: href
                });
            });
        }

        this.$tabs.trigger($.Event('shown-accordion.bs.tabcollapse'));
    };

    TabCollapse.prototype._panelHeadingToTabHeading = function($heading) {
        var href = $heading.attr('href').replace(/-collapse$/g, '');
        $heading.attr({
            'data-toggle': 'tab',
            'href': href,
            'data-parent': ''
        });
        return $heading;
    };

    TabCollapse.prototype._tabHeadingToPanelHeading = function($heading, groupId, parentId, active) {
        $heading.addClass('js-tabcollapse-panel-heading ' + (active ? '' : 'collapsed'));
        $heading.attr({
            'data-toggle': 'collapse',
            'data-parent': '#' + parentId,
            'href': '#' + groupId
        });
        return $heading;
    };

    TabCollapse.prototype._checkStateOnResize = function(){
        var view = this;
        $(window).resize(function(){
            clearTimeout(view._resizeTimeout);
            view._resizeTimeout = setTimeout(function(){
                view.checkState();
            }, 100);
        });
    };


    TabCollapse.prototype._initAccordion = function(){
        var randomString = function() {
            var result = "",
                possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for( var i=0; i < 5; i++ ) {
                result += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return result;
        };

        var srcId = this.$tabs.attr('id'),
            accordionId = (srcId ? srcId : randomString()) + '-accordion';

        this.$accordion = $('<div class="panel-group ' + this.options.accordionClass + '" id="' + accordionId +'"></div>');
        this.$tabs.after(this.$accordion);
        this.$tabs.addClass(this.options.tabsClass);
        this.getTabContentElement().addClass(this.options.tabsClass);
    };

    TabCollapse.prototype._createAccordionGroup = function(parentId, $heading){
        var tabSelector = $heading.attr('data-target'),
            active = $heading.data('bs.tabcollapse.parentLi').is('.active');

        if (!tabSelector) {
            tabSelector = $heading.attr('href');
            tabSelector = tabSelector && tabSelector.replace(/.*(?=#[^\s]*$)/, ''); //strip for ie7
        }

        var $tabPane = $(tabSelector),
            groupId = $tabPane.attr('id') + '-collapse',
            $panel = $(this.options.accordionTemplate($heading, groupId, parentId, active));
        $panel.find('.panel-heading > .panel-title').append(this._tabHeadingToPanelHeading($heading, groupId, parentId, active));
        $panel.find('.panel-body').append($tabPane.contents().detach())
            .data('bs.tabcollapse.tabpane', $tabPane);

        return $panel;
    };



    // TABCOLLAPSE PLUGIN DEFINITION
    // =======================

    $.fn.tabCollapse = function (option) {
        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('bs.tabcollapse');
            var options = $.extend({}, TabCollapse.DEFAULTS, $this.data(), typeof option === 'object' && option);

            if (!data) $this.data('bs.tabcollapse', new TabCollapse(this, options));
        });
    };

    $.fn.tabCollapse.Constructor = TabCollapse;


}(window.jQuery);

/*адаптивная верстка*/

if(($("body").hasClass("frontend_trainingcategory_show")) || ($("body").hasClass("frontend_index_index"))){
    $("body").addClass("adaptive-on"); 
}

if (($("body").hasClass("frontend_articles_index")) || ($("body").hasClass("frontend_articles_show")) || ($("body").hasClass("frontend_news_index")) || ($("body").hasClass("frontend_news_show")) || ($("body").hasClass("frontend_articles_tags")) || ($("body").hasClass("frontend_programs_index")) || ($("body").hasClass("frontend_diets_index")) || ($("body").hasClass("frontend_programs_show")) || ($("body").hasClass("frontend_diets_show")) || ($("body").hasClass("frontend_exercises_show")) || ($("body").hasClass("frontend_clubs_index")) || ($("body").hasClass("frontend_clubs_show")) || ($("body").hasClass("adaptive-on")) || ($("body").hasClass("frontend_trainingcategory_show")))  {

    /*Фикс для прокрутки мобильных браузеров*/
    function bodyFix() {
        var offset = document.body.scrollTop;
        document.body.style.top = (offset * -1) + 'px';
        document.body.classList.add('modal--opened');
    }

    function bodyFixRemove() {
        var offset = parseInt(document.body.style.top, 10);
        document.body.classList.remove('modal--opened');
        document.body.scrollTop = (offset * -1);
    }
    /*Конец Фикс для прокрутки мобильных браузеров*/
    /*Проверка адаптивности страниц*/
    /*Мобильное меню*/
    /*адаптивное меню*/
    var slideoutLeft = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'padding': 256,
        'tolerance': 70
    });
    function scrollbarWidth() {
        var documentWidth = parseInt(document.documentElement.clientWidth);
        var windowsWidth = parseInt(window.innerWidth);
        var scrollbarWidth = windowsWidth - documentWidth;
        return scrollbarWidth;
    }
    $('document').ready(function() {

        // Начальное состояние
        responsiveLeftMenu();

        $(window).resize(function() {
            /**
             * При изменении ширины окна
             */
            responsiveLeftMenu();
        });

        window.onorientationchange = function() {
            /**
             * При смене ориентации с портретной на пейзажную или обратно
             */
            responsiveLeftMenu();
        };

        function responsiveLeftMenu() {
            /*Фикс для прокрутки мобильных браузеров*/
            function bodyFix() {
                var offset = document.body.scrollTop;
                document.body.style.top = (offset * -1) + 'px';
                document.body.classList.add('modal--opened');
            }

            function bodyFixRemove() {
                var offset = parseInt(document.body.style.top, 10);
                document.body.classList.remove('modal--opened');
                document.body.scrollTop = (offset * -1);
            }
            var w = $(window).width();
            if (w <= 1080) {
                var menu_el = document.getElementById('menu'), new_menu_el = menu_el.cloneNode(true);
                var panel_el = document.getElementById('panel');
                document.body.insertBefore(new_menu_el, panel_el);
                var parent_el = menu_el.parentElement;
                parent_el.removeChild(menu_el);
                slideoutLeft.disableTouch();

                slideoutLeft.on('open', function () {
                    /*$('#dropdown-user-menu-right').removeClass( 'open');*/
                    if (document.getElementById('dropdown-user-menu-right')) {
                        slideoutRight.disableTouch();
                    }
                    slideoutLeft.enableTouch();
                    $('#search').removeClass('open');
                    $('.auth-popup').removeClass('show');
                    $('.navbar-fixed-top').removeClass('auth_mod');
                    $('body').addClass('scroll');
                    if (document.getElementById('dropdown-user-menu-right')) {
                        $("body").removeClass("fixed-open-right");
                    }

                });
                slideoutLeft.on('close', function () {
                    $('#toggle-button-id').removeClass('close');
                    document.querySelector('body').classList.remove('fixed-open-left');
                    $('body').removeClass('scroll');
                    $("body").removeClass("fixed-open-left");
                    slideoutLeft.disableTouch();
                    $('body').removeClass('left_open_block');
                    $('body').removeClass('disable-scrolling');
                    bodyFixRemove();

                });
                slideoutLeft.on('beforeopen', function () {
                    document.querySelector('.fixed').classList.add('fixed-open');
                    $('body').addClass('left_open_block');
                    $('body').removeClass('right_open_block');
                    if (document.getElementById('dropdown-user-menu-right')) {
                        $("#navbar-collapse-1").addClass("display_block");
                    }
                    $('#toggle-button-id').addClass('close');
                    $('#mask').removeClass('display_block');
                    $('body').removeClass('show-authorization');
                    document.querySelector('body').classList.add('fixed-open-left');
                    slideoutLeft.disableTouch();
                    bodyFix();
                });
                slideoutLeft.on('beforeclose', function () {
                    document.querySelector('.fixed').classList.remove('fixed-open');
                    $('#toggle-button-id').removeClass('close');
                    $('#mask').removeClass('display_block');
                    $('body').removeClass('scroll');


                });
            }


            var ws = $(window).width();
            if (ws >= '1081'-scrollbarWidth()) {
                $('#menu').appendTo('.navbar-header-menu-wrapper .row');
            }

        }

    });

    /*Правое меню*/
    if (document.getElementById('dropdown-user-menu-right')) {
        var slideoutRight = new Slideout({
            'panel': document.getElementById('panel'),
            'menu': document.getElementById('menu-user-right'),
            'padding': 256,
            'tolerance': 70,
            'side': 'right'

        });
        $('document').ready(function() {

            // Начальное состояние
            responsiveRightMenu();

            $(window).resize(function () {
                /**
                 * При изменении ширины окна
                 */
                responsiveRightMenu();
            });

            window.onorientationchange = function () {
                /**
                 * При смене ориентации с портретной на пейзажную или обратно
                 */
                responsiveRightMenu();
            };

            function responsiveRightMenu() {
                function bodyFix() {
                    var offset = document.body.scrollTop;
                    document.body.style.top = (offset * -1) + 'px';
                    document.body.classList.add('modal--opened');
                }

                function bodyFixRemove() {
                    var offset = parseInt(document.body.style.top, 10);
                    document.body.classList.remove('modal--opened');
                    document.body.scrollTop = (offset * -1);
                }

                var w = $(window).width();
                if (w <= '1080') {
                    var menuleft_el = document.getElementById('menu-user-right'), new_menuleft_el = menuleft_el.cloneNode(true);
                    var panelleft_el = document.getElementById('panel');
                    document.body.insertBefore(new_menuleft_el, panelleft_el);
                    var parentleft_el = menuleft_el.parentElement;
                    parentleft_el.removeChild(menuleft_el);


                    slideoutRight.disableTouch();
                    slideoutRight.on('open', function () {
                        slideoutLeft.disableTouch();
                        slideoutRight.enableTouch();
                        $('body').addClass('scroll');
                        $('#search').removeClass('open');
                        $('.auth-popup').removeClass('show');
                        $('.navbar-fixed-top').removeClass('auth_mod');
                        document.querySelector('body').classList.add('fixed-open-right');

                    });
                    slideoutRight.on('close', function () {
                        slideoutRight.disableTouch();
                        slideoutLeft.disableTouch();
                        document.querySelector('body').classList.remove('fixed-open-right');
                        $('body').removeClass('right_open_block');
                        $('body').removeClass('disable-scrolling');
                        bodyFixRemove();


                    });
                    slideoutRight.on('beforeopen', function () {
                        slideoutRight.disableTouch();
                        $('#toggle-button-id').removeClass('close');
                        $('#mask').removeClass('display_block');
                        $('body').removeClass('show-authorization');
                        $('body').addClass('right_open_block');
                        $('body').removeClass('left_open_block');
                        bodyFix();
                    });

                    slideoutRight.on('beforeclose', function () {
                        slideoutRight.disableTouch();
                        $('body').removeClass('scroll');
                    });


                }
                if (w >= '1081') {
                    $('#menu-user-right').appendTo('#dropdown-user-menu-right');
                }
                /*Конец правого меню*/
            }
        });
    }

    $(function () {
        $('a[href="#search"]').on('click', function (event) {
            event.preventDefault();
            $('#search').addClass('open');
            $('#search > form > input[type="search"]').focus();
            $('body').addClass('overflov-hidden');
            $('.auth-popup').removeClass('show');
            $('.navbar-fixed-top').removeClass('auth_mod');
            if (document.getElementById('dropdown-user-menu-right')) {
                if (slideoutLeft.isOpen() || slideoutRight.isOpen()) {
                    slideoutLeft.close();
                    slideoutRight.close();
                } else {
                    bodyFix();
                }
            } else {
                if (slideoutLeft.isOpen()) {
                    slideoutLeft.close();
                } else {
                    bodyFix();
                }
            }
        });

        $('#search, #search button.close').on('click keyup', function (event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
                $('body').removeClass('overflov-hidden');
                bodyFixRemove();
            }
        });
    });
    jQuery(window).scroll(function () {
        var the_top = jQuery(document).scrollTop();
        if (the_top > 50) {
            jQuery('body').addClass('fixed-menu');
        }
        else {
            jQuery('body').removeClass('fixed-menu');
        }
    });
    var usernotices = document.getElementById("dropdown-user-notices-right");
    if (usernotices != null) usernotices.onclick = function () {
        slideoutLeft.close();
        if (document.getElementById('dropdown-user-menu-right')) {
            slideoutRight.close();
        }
        $('#search').removeClass('open');
        $('.auth-popup').removeClass('show');
        $('.navbar-fixed-top').removeClass('auth_mod');
    };
    var user_messages_dropdown = document.getElementById("user-messages-dropdown");
    if (user_messages_dropdown != null) user_messages_dropdown.onclick = function () {
        slideoutLeft.close();
        if (document.getElementById('dropdown-user-menu-right')) {
            slideoutRight.close();
        }
        $('#search').removeClass('open');
        $('.auth-popup').removeClass('show');
        $('.navbar-fixed-top').removeClass('auth_mod');
    };
    $('document').ready(function() {

        // Начальное состояние
        minicartEvoHide();


        $(window).resize(function() {
            /**
             * При изменении ширины окна
             */
            minicartEvoHide();
        });

        window.onorientationchange = function() {
            /**
             * При смене ориентации с портретной на пейзажную или обратно
             */
            minicartEvoHide();

        };



        function minicartEvoHide() {
            if ($('.minicart-evo').find('*').length == 0) {
                $('.minicart-evo').remove();
            }
        }
    });
    $('document').ready(function() {

        // Начальное состояние

        responsiveLeftMenuOn();
        if (document.getElementById('dropdown-user-menu-right')) {
            responsiveRightMenuOn();
        }


        $(window).resize(function() {
            /**
             * При изменении ширины окна
             */


        });

        window.onorientationchange = function() {
            /**
             * При смене ориентации с портретной на пейзажную или обратно
             */


        };




    });
    function responsiveLeftMenuOn() {
        document.querySelector('.toggle-button').addEventListener('click', function () {
            if (document.getElementById('dropdown-user-menu-right')) {
                if (slideoutRight.isOpen()) {
                    $("#dropdown-user-menu-right").removeClass("open");
                    slideoutRight.close();
                    slideoutLeft.close();
                } else {
                    $("#toggle-button-id").toggleClass("close");
                    slideoutLeft.toggle();
                    var wds = $(window).width();
                    if (wds >= '1080') {
                        $('body').toggleClass('disable-scrolling');
                    }
                }
            } else {
                $("#toggle-button-id").toggleClass("close");
                slideoutLeft.toggle();
                var wds = $(window).width();
                if (wds >= '1080') {
                    $('body').toggleClass('disable-scrolling');
                }
            }


        });
    }
    function responsiveRightMenuOn() {
        var wds = $(window).width();
        document.querySelector('.toggle-button-right').addEventListener('click', function () {
            if (slideoutLeft.isOpen()) {
                if (document.getElementsByClassName('fixed-open-left')) {
                    slideoutLeft.close();
                    $("body").removeClass("fixed-open-left");

                }
                if (wds >= '1080') {
                    $('.footer').appendTo("#panel");
                    $('.scrollup').appendTo("#panel");

                }
                slideoutLeft.close();
                slideoutRight.close();
            } else {
                document.querySelector('body').classList.toggle('fixed-open-right');
                slideoutRight.toggle();
                slideoutRight.disableTouch();
                //$("#menu-user-right").toggle("display_block");
                $('body').toggleClass('disable-scrolling');
                /*правка scroll to top*/

                if (wds >= '1080') {
                    $('.scrollup').appendTo("body");
                    $('.footer').appendTo("body");
                }

            }
        });



    }



    /*ДЛя адаптивных блоков*/

    $('document').ready(function() {

        // Начальное состояние
        articleResponsiveCategory();
        articleResponsiveTag();

        $(window).resize(function() {
            /**
             * При изменении ширины окна
             */
            articleResponsiveCategory();
            articleResponsiveTag();
        });

        window.onorientationchange = function() {
            /**
             * При смене ориентации с портретной на пейзажную или обратно
             */
            articleResponsiveCategory();
            articleResponsiveTag();
        };

        function articleResponsiveCategory() {
            var w = $(window).width();
            if (w <= '767') {
                if ($('#articles-category').find('*').length == 0) {
                    /*перемещение блоков категорий*/
                    $('.hidden-xs .evo-grey-aside-title').appendTo('#articles-category');
                    $('.hidden-xs .evo-category-nav').appendTo('#articles-category');

                }
            }
            if (w >= '768') {
                if ($('#category-right-colomn').find('*').length == 0) {
                    $('.visible-xs .evo-grey-aside-title').appendTo('#category-right-colomn');
                    $('.visible-xs .evo-category-nav').appendTo('#category-right-colomn');


                }
            }
        }
        function articleResponsiveTag() {
            var w = $(window).width();
            if (w <= '767') {
                /*перемещение блоков тегов*/
                if ($('#tag_responsive').find('*').length == 0) {
                    $('.hidden-xs .evo-r-side-title').appendTo('#tag_responsive');
                    $('.hidden-xs .evo-r-side-p-container').appendTo('#tag_responsive');
                }
            }
            if (w >= '768') {
                if ($('#tag_desktop').find('*').length == 0) {
                    $('.visible-xs .evo-r-side-title').appendTo('#tag_desktop');
                    $('.visible-xs .evo-r-side-p-container').appendTo('#tag_desktop');
                }
            }
        }
    });





}


if($(document).width() < 992){
    $(".filter-header").click(function(){
        $(this).toggleClass("open");
        $(this).parent().find(".filter-list").slideToggle();
    });
}


//адаптвиные вкладки
if (($("body").hasClass("frontend_programs_show")) || ($("body").hasClass("frontend_diets_show"))) {

    function scrollbarWidth() {
        var documentWidth = parseInt(document.documentElement.clientWidth);
        var windowsWidth = parseInt(window.innerWidth);
        var scrollbarWidth = windowsWidth - documentWidth;
        return scrollbarWidth;
    }




    // $('#nav-list-vivid-tabs-accordion').collapse({
    //     toggle: false
    // })
    // $('#nav-list-vivid-tabs-accordion').on('show.bs.collapse', function () {
    //     $('#nav-list-vivid-tabs-accordion .in').collapse('hide');
    // });
    // $(".collapse.in").removeClass('collapse in');
    // $(document).on("shown.bs.collapse shown.bs.tab", ".panel-collapse, a[data-toggle='tab']", function (e) {
    //
    // });


    $('document').ready(function() {

        // Начальное состояние
        responsiveTabsPrograms();
        accordionOnPrograms();
        responsiveTabsProgramsPit();
        accordionOnProgramsPit();


        $(window).resize(function () {
            /**
             * При изменении ширины окна
             */
            responsiveTabsPrograms();
            responsiveTabsProgramsPit();
        });

        window.onorientationchange = function () {
            /**
             * При смене ориентации с портретной на пейзажную или обратно
             */
            responsiveTabsPrograms();
            responsiveTabsProgramsPit();

        };
    });
//табы для блока с программами
    function responsiveTabsPrograms() {
        var i = 0;
        var count = 30;
        var w = $(window).width();
        if (w >= '768'-scrollbarWidth()) {
            for (var i = 0; i < count; i++) {
                var desktopVididTabsTitle = $('#tabs-zone_' + i + '').children('.week-title_' + i + '');
                if (desktopVididTabsTitle) {
                    $('#tabs-zone_' + i + ' .week-title_' + i + '').insertBefore($('.col-sm-3 #week_'+i+' .tab_position_desktop'));
                }
                var navListVividTabs = $("#tabs-zone_"+i+"").children('#nav-list-vivid-tabs_'+i+'');
                if (navListVividTabs) {
                    $('#nav-list-vivid-tabs_'+i+'').insertBefore($('.col-sm-3 #week_'+i+' .tab_position_desktop'));
                }
            }

        }

        if (w <= '767'-scrollbarWidth()) {
            for (var i = 0; i < count; i++) {
                var responsiveVididTabsTitle = $(".col-sm-3 #week_" + i + "").children('.week-title_' + i + '');
                if (responsiveVididTabsTitle) {
                    $('#week_' + i + ' .week-title_' + i + '').insertBefore($('#tabs-zone_' + i + ' #tab-content-zoneContent_' + i + ''));

                }
                var responsiveVididTabs = $(".col-sm-3 #week_" + i + "").children('#nav-list-vivid-tabs_' + i + '');
                if (responsiveVididTabs) {
                    // $('#week_' + i + '').insertBefore($('#tabs-zone_' + i + ' #tab-content-zoneContent_' + i + ''));
                    $('#nav-list-vivid-tabs_' + i + '').insertBefore($('#tabs-zone_' + i + ' #tab-content-zoneContent_' + i + ''));

                    $('#nav-list-vivid-tabs_' + i + ' li a').attr("data-toggle", "tab");


                }


            }


        }


    }
    function accordionOnPrograms() {
        var i = 0;
        var count = 30;
        for (var i = 0; i < count; i++) {
            $('#nav-list-vivid-tabs_'+i+'').tabCollapse({
                tabsClass: 'hidden-xs',
                accordionClass: 'visible-xs'

            });
        }

    }

    //табы для блока с питанием
    function responsiveTabsProgramsPit() {
        var wd = $(window).width();
        if (wd >= '768'-scrollbarWidth()) {
            var navListVividTabsPit = $("#tabs-zone-pit").children('#nav-list-vivid-tabs-pit');
            if (navListVividTabsPit) {
                $('#nav-list-vivid-tabs-pit').insertBefore($('.col-sm-3 .calendar-week-diets .marginTop:first'));
            }


        }

        if (wd <= '767'-scrollbarWidth()) {
            var responsiveVididTabsPit = $(".col-sm-3 .calendar-week").children('#nav-list-vivid-tabs-pit');
            if (responsiveVididTabsPit) {
                $('#nav-list-vivid-tabs-pit').insertBefore($('#tabs-zone-pit #tab-content-zoneContent-pit'));
                $('#nav-list-vivid-tabs-pit li a').attr("data-toggle", "tab");

            }

        }

    }
    function accordionOnProgramsPit() {
        $('#nav-list-vivid-tabs-pit').tabCollapse({
            tabsClass: 'hidden-xs',
            accordionClass: 'visible-xs'
        });
    }

    $(window).load(function () {
        var $elemday = $("#nav-list-vivid-tabs-accordion .panel-default"); //здесь находим день
        var $elemspan = $("#nav-list-vivid-tabs-accordion .panel-default .arrow"); //здесь находим есть ли что в каком-то дне
        $elemday.has($elemspan).addClass("panel__active");
    });
}
/*Страница тренеровок*/
$(document).ready(function ($) {
    if ($("body").hasClass("frontend_exercises_show")) {

        $('document').ready(function() {

            // Начальное состояние
            w100adaprive();
            youtubeadaprive();
            youtubeadaprivewrap();
            rateMobileSlider();
            responsiveExercisesInfo();
            $(window).resize(function () {
                /**
                 * При изменении ширины окна
                 */
                w100adaprive();
                youtubeadaprive();
                youtubeadaprivewrap();
                rateMobileSlider();
                responsiveExercisesInfo();
            });

            window.onorientationchange = function () {
                /**
                 * При смене ориентации с портретной на пейзажную или обратно
                 */
                w100adaprive();
                youtubeadaprive();
                youtubeadaprivewrap();
                rateMobileSlider();
                responsiveExercisesInfo();

            };
        });
        function w100adaprive() {
            var k = 790 / 548;
            var widthFlexW100 = $(window).width();
            $('.tech-media-block-flex .flex.w100').height($('.tech-media-block-flex .flex.w100').width() / k);
            if (widthFlexW100 <= '1210'-scrollbarWidth()) {
                $(window).resize(function () {
                    $('.tech-media-block-flex .flex.w100').height($('.tech-media-block-flex .flex.w100').width() / k);
                });
            }
        }
        function responsiveExercisesInfo() {
            var w = $(window).width();
            if (w <= '768') {

                /*перемещение блоков категорий*/
                $('.upr__info__wrap').appendTo('#responsive-info');

            }
            if (w <= '767') {

                /*перемещение блоков категорий*/
                $('#responsive-info .upr__info__wrap').insertBefore('.training_item_mod');

            }
            if (w >= '769') {

                $('#responsive-info .upr__info__wrap').insertBefore('.training_item_mod');

            }
        }

        /*Уменьшаем размер видео*/
        function youtubeadaprivewrap() {
            var ku = 16 / 9;
            var widthYoutube = $(window).width();
            $('.tech-media-block-flex .flex.w100 .iframe_wrap').height($('.tech-media-block-flex .flex.w100 iframe').width() / ku);

            $(window).resize(function () {
                $('.tech-media-block-flex .flex.w100 .iframe_wrap').height($('.tech-media-block-flex .flex.w100 iframe').width() / ku);
            });
        }
        function youtubeadaprive() {
            var ku2 = 16 / 9;
            // var widthYoutubeWrap = $(window).width();
            $('.tech-media-block-flex .flex.w100 iframe').height($('.tech-media-block-flex .flex.w100 iframe').width() / ku2);

            $(window).resize(function () {
                $('.tech-media-block-flex .flex.w100 iframe').height($('.tech-media-block-flex .flex.w100 iframe').width() / ku2);
            });
        }
        $( ".no-hide.get-rating-slider" ).click(function() {
            $('.rate-block-bottom .get-rating-slider').hide();
        });
        /*Рейтинг на мобильных*/
        function rateMobileSlider() {
            var ratescreensize = $(window).width();
            var $rateslider = $('.rate-block-bottom .rating_slider');
            var $sliderTextOn = $('.rate-block-wrap-social').has('.slidertext');

            var $slidertexthtml = $('.slidertext');
            if (ratescreensize<=767) {
                $('.inner').append('<p>***</p>');

                $rateslider.insertAfter('.slidertext');
                body.on('click', '.get-rating-slider', function(){
                    $(this).show();
                    $(".rating_slider").show();
                    $(".rate-block-wrap-social-head").hide();
                    $(".rate-block-wrap-social-script").hide();
                    $slidertexthtml.show();
                });
                $( ".no-hide.get-rating-slider" ).click(function() {
                    $('.rate-block-bottom .get-rating-slider').show();
                });
                if ($('.rate-block-wrap-social').has($('.rating_slider'))) {
                    $('.rate-block-bottom .get-rating-slider').show();
                }
            }
            if (ratescreensize>=768) {

                if ($rateslider) {
                    $rateslider.appendTo($('.rate-block-bottom'));
                    $('.rate-block-bottom .rating-slider').hide();

                }
            }

        }



    }

});