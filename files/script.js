var body,
    mainSlider,
    hasMainSlider = false
    ;

var ingredientsData = {};

lazyLoad();
generateSvg();
dropdownsPosition();
matchHeight();
ajaxComments();
ajaxProgramscalendar();
ajaxDietscalendar();

$(document).on("keypress", 'form:not(.enter-posting)', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        return false;
    }
});

$(document).ready(function ($) {
    body = $('body');
    //override defaults
    alertify.defaults.transition = "zoom";
    alertify.defaults.glossary.title = "IQ BODY";

    $('.onlyNumber').ForceNumericOnly();

    $('.time-mask').mask('99:99:99');

    $(".select, .multiselect").select2({
        language: "ru",
        dropdownCssClass: 'dropdown-inverse'
    });
    $(".tagsinput").tagsinput();

    setupBasicUpload();
    setupAjaxTabs();

    body.on("change", "select.submitOnChange", function(){
        $(this).closest("form").submit();
    });

    body.on("click", ".openMinimodal", function(){
        $(".minimodal-evo").hide();

        var $this = $(this);
        var $parent = $this.closest($this.data("parent"));

        $($this.data("selector"), $parent).show();
        return false;
    });

    body.on("click", ".closeMinimodal", function(){
        var $this = $(this);
        var $parent = $this.closest(".minimodal-evo");

        $parent.hide();
        return false;
    });

    if($(".ajaxExResultCounter").size() > 0) {
        loadExResults();
    }

    body.on("submit", ".addExResultForm", function(){
        var $form = $(this);
        var parent = $form.closest(".addExResultContainer");

        $.post("/ajax/ajaxaddresults", $form.serializeArray(), function(data){
            if(data.response.success) {
                $(".ajaxExResultCounter", parent).html(data.response.html);
                alertify.success("Результаты добавлены");
                alertify.closeAll();

                $('[data-toggle="tooltip"]').tooltip();
            }
            else {
                alertify.error("Произошла ошибка");
                alertify.closeAll();
            }

            $(".minimodal-evo").hide();
        }, "json");

        return false;
    });

    if($(".clickLoad").size() > 0) {
        $(".clickLoad").each(function(){
            $(this).click().removeClass("clickLoad");
            return false;
        });
    }
    body.on('click', '.openHide', function () {
        var $this = $(this);
        var selector = $this.data("selector");

        if($this.hasClass("active")) {
            $this.removeClass("active");
            if($this.data("changetext") != undefined) {
                $this.text($this.data("text"));
            }
        }
        else {
            $this.addClass("active");
            if($this.data("changetext") != undefined) {
                $this.text($this.data("changetext"));
            }

            if($this.data("remove")) {
                $this.remove();
            }
        }

        $(selector).toggleClass("hide");

        return false;
    });

    body.on('click', '.sendSms', function () {
        $.post("/ajax/ajaxsendsms", $(this).data(), function(data){
            if(data.response.code == 100) {
                alertify.success("Отправлено");
                alertify.closeAll();
            }
            else {
                alertify.error("Произошла ошибка");
                alertify.closeAll();
            }
        }, "json");

        return false;
    });

    body.on('change', '.fastChange', function () {
        var dataArr = $(this).data();
        dataArr.value = $(this).val();

        $.post("/ajax/ajaxfastchange", dataArr, function(data){
            if(data.success) {
                alertify.success("Сохранено");
                alertify.closeAll();
            }
            else {
                alertify.error("Произошла ошибка");
                alertify.closeAll();
            }
        }, "json");
    });

    body.on('submit', 'form.form-ajax', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});

        var error = false;
        var form = $(this);

        $('.required', form).removeClass('has-error');
        $('.required', form).each(function(){
            if($(this).val() == '') {
                error = true;
                $(this).addClass('has-error');
            }
        });

        if(!error){
            $.ajax({
                url: '/index/ajaxform',
                data: form.serializeArray(),
                success: function(data){
                    if(data.response.code == 1){
                        $('input:not(:submit)', form).val('');
                        $('textarea', form).val('');

                        alertify.success(data.response.message);
                        alertify.closeAll();
                    }
                    else {
                        alertify.error(data.response.error);
                    }
                    $.unblockUI();
                },
                type: "POST", dataType: "json"
            });
        }
        else {
            alertify.error('Заполните обязательные поля');
            $.unblockUI();
        }


        return false;
    });


    body.on("click", ".showHideBlocks", function(){
        var $this = $(this);
        var selector = $this.data("selector");

        $(selector).show();
        $this.remove();

        return false;
    });

    body.on("click", ".simpleRemove", function(){
        var $this = $(this);
        var data = $this.data();

        $.blockUI({message: '<h2>Подождите...</h2>'});

        $.post("/ajax/ajaxsimpleremove", data, function(json){
            if(json.response.success) {
                $this.closest(data.parent).remove();
            }

            $.unblockUI();
        }, "json");


        return false;
    });

    body.on('submit', 'form.validate-form', function(){
        var form = $(this);
        var requiredError = false;

        $('.required', form).removeClass('has-error');
        $('.required', form).removeClass('has-success');

        $('.required', form).each(function(){
            var type = $(this).attr('type');

            if((type == 'text' || type == 'email' || type == 'tel' || type == 'password') && $(this).val() == '') {
                requiredError = true;
                $(this).addClass('has-error');
            }
            else {
                $(this).addClass('has-success');
            }

            if((type == 'radio' || type == 'checkbox') && !$(this).prop('checked')) {
                requiredError = true;
                $(this).addClass('has-error');
            }
            else {
                $(this).addClass('has-success');
            }
        });


        if(!requiredError) {
            return true;
        }
        else {
            $(".has-error", form).first().focus();
            alertify.warning('Заполните все обязательные поля');
        }

        return false;
    });

    body.on('keyup', '#searchIngredients', function(){
        var $this = $(this);
        var value = $this.val();
        var parent = $this.closest('.searchIngredientsContainer');
        var dropdown = $('.searchIngredientsDropdown', parent);
        var dropdownHtml = "";
        dropdown.html("");
        dropdown.hide();

        if(value.length >= 2) {
            $.post('/ajax/ajaxgetingredients', {query: value}, function(data){
                if(data.response.code) {
                    $.each(data.response.items, function(index, value){
                        ingredientsData[value.id] = value;
                        dropdownHtml = dropdownHtml + "<div data-id='"+value.id+"' class='selectIngredient'>" +
                            "<img src='/uploads/fx/50x50/ingredients/original/"+value.thumbnail+"' width='50' height='50'/>"+value.title+"" +
                            "</div>";
                    });

                    dropdown.html(dropdownHtml);
                    dropdown.show();
                }
            }, "json");
        }

        return false;
    });

    body.on('click', '.editInfo', function(){
        var $this = $(this);
        var $text = $this.find('.editInfoText');
        var type = $this.data('type');

        $('.program-info-block').toggleClass("hide");

        if(type == 'show') {
            $text.text('Скрыть информацию');
            $this.data('type', 'hide');
        }
        if(type == 'hide') {
            $text.text('Редактировать информацию');
            $this.data('type', 'show');
        }
    });

    if(window.location.hash == "#payment_code_1") {
        alertify.success("Оплата произведена успешно");
    }
    if(window.location.hash == "#payment_code_0") {
        alertify.error("Оплата не произведена");
    }

    body.on("click", '.openCreateBalance', function () {
        alertifySize();
        alertify.dialog('confirm')
            .set({
                'title' : "Пополнить баланс",
                'labels':{ok:'Пополнить', cancel:'Отмена'},
                'message': '<form action="/kassa/proccess" method="post" id="balance_form"><div class="form-group input-group col-xs-12">' +
                '<input type="hidden" name="order_id" value="'+parseInt(Math.random() * (999999 - 999) + 999)+'">' +
                '<input type="hidden" name="type" value="balance">' +
                '<input type="hidden" name="desc" value="Пополнение баланса">' +
                '<label for="sum">Сумма пополнения</label>' +
                '<input type="text" class="form-control spinner" name="sum" value="100"></div>' +
                '</form>',
                'modal': true,
                'onshow': function(){
                    var form = $("#balance_form");

                    $( ".spinner", form).spinner({ min: 10, step: 10});
                },
                'onok': function(){
                    var form = $("#balance_form");
                    if($("input[name='sum']", form).val() != "") {
                        $("#balance_form").submit();
                    }
                    else {
                        alertify.warning("Заполните обязательное поле");
                        return false;
                    }

                },
                'oncancel': function(){

                }
            }).show();
    });

    body.on('click', '.btnLikes', function(){
        var $this = $(this);
        var parent = $this.closest('.likesBlock');
        var iconLikesClass = "likeIcon fa fa-thumbs-o-up";
        var iconLikesClassVoted = "likeIcon fa fa-thumbs-up";
        var iconDisLikesClass = "likeIcon fa fa-thumbs-o-down";
        var iconDisLikesClassVoted = "likeIcon fa fa-thumbs-down";

        $.blockUI({message: '<h2>Подождите...</h2>'});
        $.post('/ajax/ajaxlikes', $this.data(), function(data){
            if(data.success) {
                $('.likes .ldCount', parent).text(data.likes);
                $('.dislikes .ldCount', parent).text(data.dislikes);

                alertify.success(data.message);
                $('.btnLikes', parent).addClass("hasVote");

                if(data.resultType == "likes") {
                    $(".likes .likeIcon", parent).attr("class", iconLikesClassVoted);
                    $(".dislikes .likeIcon", parent).attr("class", iconDisLikesClass);
                }
                if(data.resultType == "dislikes") {
                    $(".dislikes .likeIcon", parent).attr("class", iconDisLikesClassVoted);
                    $(".likes .likeIcon", parent).attr("class", iconLikesClass);
                }
                if(data.resultType == "unlikes") {
                    $(".likes .likeIcon", parent).attr("class", iconLikesClass);
                }
                if(data.resultType == "undislikes") {
                    $(".dislikes .likeIcon", parent).attr("class", iconDisLikesClass);
                }
            }
            else {
                alertify.warning(data.error);
            }

            $.unblockUI();
        }, "json");


        return false;
    });

    body.on('click', '.bestQa', function(){
        var $this = $(this);

        $.blockUI({message: '<h2>Подождите...</h2>'});
        $.post('/qa/ajaxbest', $this.data(), function(data){
            if(data.success) {
                $('.bestQaBlock').removeClass('hide');
                $('.bestQaBlockMsg').html(data.html);
                alertify.success(data.message);
            }
            else {
                alertify.warning(data.error);
            }

            $.unblockUI();
        }, "json");


        return false;
    });

    body.on('click', '.myCoach', function(){
        var $this = $(this);
        var parent = $this.closest('.myCoachBlock');

        $.blockUI({message: '<h2>Подождите...</h2>'});
        $.post('/ajax/ajaxmycoach', $this.data(), function(data){
            if(data.success) {
                alertify.success(data.message);
                if($this.data('type') == 'remove') {
                    $this.remove();
                    parent.remove();
                }
                if($this.data('type') == 'add') {
                    $this.remove();
                }
            }
            else {
                alertify.warning(data.error);
            }

            $.unblockUI();
        }, "json");


        return false;
    });

    var $fulltextareainstance = $('.fulltextarea').data('instance');
    $('.fulltextarea').redactor({
        //lang: 'ru',
        plugins: ["video", "table", "imagemanager", "fontcolor", "fontsize", "fullscreen"],
        imageUpload: '/'+$fulltextareainstance+'/ajaxredactoruploader',
    });

    $('.basictextarea').redactor({
        //lang: 'ru',
        plugins: ["video", "table", "imagemanager", "fontcolor", "fontsize", "fullscreen"],
        imageUpload: '/ajax/ajaxbasicredactoruploader',
    });

    setupCommentsEditor();

    var commentareaeditor = $('#respond .redactor-editor');
    commentareaeditor.html('');

    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $('.datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd.mm.yy',
        maxDate: "+0M +0D"
    });

    $('.datepicker-future').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd.mm.yy',
        minDate: "+0M +0D"
    });

    tabContent();

    $(".fb").fancybox({
        helpers : {
            overlay: {
                locked: false
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    $( ".spinner" ).spinner({ min: 1 });
    $(".disabledCount").spinner( "disable" );

    if($("#price_slider").size() > 0) {
        var priceSlider = $("#price_slider");
        priceSlider.slider({
            min: parseInt(priceSlider.data('min')),
            max: parseInt(priceSlider.data('max')),
            values: [ parseInt(priceSlider.data('from')), parseInt(priceSlider.data('to')) ],
            step: parseInt(priceSlider.data('step')),
            orientation: "horizontal",
            range: true,
            slide: function( event, ui ) {
                $.each(ui.values, function(index, value){
                    $(".price-slider-"+index).val(value);
                });
            },
            change: function( event, ui ) {
                filterShop();
            }
        });
    }

    body.on('change', '#shopFilter input', function(){
        $("#price_slider").slider( "values", [$('.price-slider-0').val(), $('.price-slider-1').val()]);

        filterShop();
    });

    body.on('click', '.changeSort', function(){
        var $this = $(this);
        var filter = $('#shopFilter');
        var field = $this.data('field');
        var sort = $this.data('sort');
        var icon = $this.data('icon');

        var nextSort = "asc";
        if(sort == "asc") nextSort = "desc";

        $this.data('sort', nextSort);
        $this.addClass('bold');

        $('.changeSortIcon', $this).attr('class', 'changeSortIcon fa fa-sort-'+icon+'-'+sort);

        $('[name="sort['+field+']"]', filter).val(sort);
        filterShop();

        return false;
    });

    function filterShop(){
        var form = $('#shopFilter');

        $('.ajax-content-more').html("");
        loading('show', '.ajax-content-more', true);
        $.post('/product/ajaxfiltershop', form.serializeArray(), function(data){
            $('.ajax-content-more').html(data);
            loading('hide');
        });
    }

    body.on('click', '.get-rating-slider', function(){
        $(this).hide();
        $(".rating_slider").show();
    });


    initRatingSlider();


    $('#programFilter').on('submit', function(){
        loading('show', '.content-filtered', true);

        var form = $(this);
        var data = form.serializeArray();

        $.post( "/ajax/ajaxfilterprogram", data, function(data){
            if(data != '' && data != 1) {
                $('.content-filtered').html(data);
                lazyLoad();
                $('.more_button').hide();
            }
            if(data == 1) {
                $('.content-filtered').html('');
                $('.more_button').show();
            }
            loading('hide');
        });

        return false;
    });

    $('#coachFilter').on('submit', function(){
        loading('show', '.content-filtered', true);

        var form = $(this);
        var data = form.serializeArray();

        $.post( "/ajax/ajaxfilterpeople", data, function(data){
            if(data != '' && data != 1) {
                $('.content-filtered').html(data);
                lazyLoad();
                $('.more_button').hide();
            }
            if(data == 1) {
                $('.content-filtered').html('');
                $('.more_button').show();
            }
            loading('hide');
        });

        return false;
    });

    $('.search-menu .search-menu-icon').click(function(){
        $(this).closest('.search-menu').toggleClass('active');
        $(this).closest('.search-menu-activity').toggleClass('active');
        $('.closedsearch').toggleClass('hide');
    });

    if($('#main-slider').size() > 0){
        hasMainSlider = true;
        mainSlider = $('.main-slider').bxSlider({
            auto: true,
            pause: 5000,
            speed: 1000,
            preloadImages: 'all',
            pager: false,
            prevText: '',
            nextText: '',
            touchEnabled: false
            /*
             animation: "slide",
             selector: ".main-slider > .main-slider-block",
             controlNav: false,
             namespace: "main-slider-",
             prevText: "",
             nextText: "",
             */
        });
    }

    if($('.simple-slider').size() > 0){
        $('.simple-slider').each(function(){
            var $this = $(this);
            $this.bxSlider({
                mode: 'fade',
                auto: $this.data("auto"),
                pause: $this.data("pause"),
                speed: $this.data("speed"),
                preloadImages: 'all',
                pager: false,
                prevText: '',
                nextText: '',
                touchEnabled: false
            });
        });
    }


    $('.changeLogPopupBtn').click(function(){
        var type = $(this).data('type');
        if(type == 'reg'){
            $('.reg-view-popup').removeClass('hide');
            $('.login-view-popup').addClass('hide');

            $('.change-reg').addClass('hide');
            $('.change-login').removeClass('hide');

            $('.login-view-popup input').prop('disababled', true);
            $('.reg-view-popup input').prop('disababled', false);
        }
        if(type == 'login'){
            $('.login-view-popup').removeClass('hide');
            $('.reg-view-popup').addClass('hide');

            $('.change-login').addClass('hide');
            $('.change-reg').removeClass('hide');

            $('.login-view-popup input').prop('disababled', false);
            $('.reg-view-popup input').prop('disababled', true);
        }
        return false;
    });

    if($('.onLoadModal').size() > 0) {
        setTimeout(function(){
            $('.onLoadModal').each(function(){
                var $this = $(this);
                var selector = $this;
                alertify.dialog('confirm')
                    .set({
                        'title' : $this.data('title'),
                        'labels': {ok: $this.data('ok'), cancel: 'Закрыть'},
                        'message': $(selector).html(),
                        'modal': false,
                        'pinnable': true,
                        'onshow': function(){
                            alertifySize($(selector));
                        },
                        'onok': function () {
                            if($this.data('link') != undefined) window.open($this.data('link'),'_blank');
                            if($this.data('form') != undefined) {
                                $($this.data('form'), '.ajs-dialog').submit();
                                return false;
                            }

                            alertify.closeAll();
                        }
                    }).show();
            });
        }, "30000");
    }


    body.on('click', '.showModal', function(){
        //alertifySize();
        var $this = $(this);
        var selector = $this.attr('href');
        alertify.dialog('confirm')
            .set({
                'title' : $this.data('title'),
                'labels': {ok: $this.data('ok'), cancel: 'Закрыть'},
                'message': $(selector).html(),
                'modal': false,
                'pinnable': false,
                'onshow': function(){
                    alertifySize($(selector));

                    if($this.data('youtube') != undefined)
                    {
                        var iframe = '<iframe id="ytplayer" width="600" height="415" src="'+$this.data('youtube')+'" frameborder="0" allowfullscreen></iframe>';
                        $('.ajs-body').html(iframe);
                    }
                },
                'onok': function () {
                    if($this.data('link') != undefined) window.open($this.data('link'),'_blank');
                    if($this.data('form') != undefined) {
                        $($this.data('form'), '.ajs-dialog').submit();
                        return false;
                    }

                    alertify.closeAll();

                    if($this.data('youtube') != undefined) {
                        $('.ajs-body').html("");
                    }
                },
                'onclose': function () {
                    if($this.data('youtube') != undefined) {
                        $('.ajs-body').html("");
                    }
                }
            }).unpin().show();
    });

    body.on('click', '#cancel-comment-edit-link', function(){
        $('.text-of-comment .text-comment').show();
        $('.comment-actions').show();
        $('.text-of-comment .edit-comment-block').html('');
    });

    body.on('click', '#cancel-comment-reply-link', function(){
        respondComplete();
    });

    body.on('click', '.editComment', function(){
        $('.comment-actions').show();
        $('.text-of-comment .text-comment').show();
        $('.text-of-comment .edit-comment-block').html('');

        var commentId = $(this).data('id');
        var level = $(this).data('level');
        var selecterAppend = '#comment-id-'+commentId;
        if(level > 0) selecterAppend = '#comment-id-c'+commentId;

        var text = $(selecterAppend+' .text-of-comment .text-comment').hide();
        $(selecterAppend+' .comment-actions').hide();

        $.post( "/ajax/ajaxgetcommenttext", {id: commentId, level: level}, function(data){
            if(data.response.code == 1) {
                var form = '<form id="editComment" class="enter-posting" method="post">' +
                    '<textarea class="form-control editcommentarea" id="comment" name="comment" aria-required="true" cols="58" rows="10" tabindex="4">'+data.response.html+'</textarea>' +
                    '<div class="comment-edit-submit text-right"><a rel="nofollow" class="label label-danger" id="cancel-comment-edit-link">Отменить</a>' +
                    '<input type="hidden" name="id" value="'+commentId+'" />' +
                    '<input type="hidden" name="level" value="'+level+'" />' +
                    '<button type="submit" id="submit" class="btn btn-info btn-lg">Сохранить</button>' +
                    '</form>';

                $(selecterAppend+' .text-of-comment .edit-comment-block').html(form);

                $('.editcommentarea').redactor({
                    focus: false,
                    //lang: 'ru',
                    plugins: ["video", "imagemanager", "clips"],
                    buttons: ['image', 'video', 'link'],
                    imageUpload: '/ajax/ajaxredactoruploader',
                    pasteBeforeCallback: function(html)
                    {
                        if(html.search("img") == -1){
                            return html;
                        }

                        if($(html).hasClass('emoji')){
                            $(html).attr('style', "");
                            this.insert.htmlWithoutClean(html);
                        }
                        else {
                            return html;
                        }
                    }
                });
            }
            else {
                alertify.error(data.response.error);
            }
        });
    });

    body.on('click', '.deleteComment', function(){
        var comCount = $('.comCount').text();
        var commentId = $(this).data('id');
        var level = $(this).data('level');
        var selecterAppend = '#comment-id-'+commentId;
        if(level > 0) selecterAppend = '#comment-id-c'+commentId;

        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Удалить', cancel:'Отмена'},
                'message': 'Вы уверены что хотите удалить комментарий' ,
                'onok': function(){
                    $.post( "/ajax/ajaxdeletecomment", {id: commentId, level: level}, function(data){
                        if(data.response.code == 1) {
                            alertify.success(data.response.message);

                            $(selecterAppend).hide('fast', function(){
                                $(selecterAppend).remove();
                                if(comCount == 1){
                                    $('.comments-title').remove();
                                }
                                else
                                {
                                    comCountFunction('minus');
                                }

                            });
                        }
                        else {
                            alertify.error(data.response.error);
                        }
                    });
                }
            }).show();


    });

    function comCountFunction(type){
        var comCount = $('.comCount').text();
        if(type == 'minus') $('.comCount').text(parseInt(comCount) - 1);
        if(type == 'plus') $('.comCount').text(parseInt(comCount) + 1);
    }

    function respondComplete(){
        $('#cancel-comment-reply-link').addClass('hide');
        var form = $('#respond');
        var comment = $('#comment', form);
        comment.val('');
        commentareaeditor.html('');

        $(form).prependTo("#comments");
        $('#comment_parent', form).val(0);
    }

    body.on('click' , '.addDietToMe', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});

        var $this = $(this);
        var id = $(this).data('p_id');

        $.post( "/profilediets/ajaxaddprogramtome", {p_id: id}, function(data){
            $.unblockUI();

            if(data.response.code == 1){
                alertify.success(data.response.result.msg);
                $this.attr('disabled', true);
            }
            else {
                alertify.error(data.response.result.msg);
            }
        });

    });

    body.on('click' , '.addProgramToMe', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});

        var $this = $(this);
        var id = $(this).data('p_id');

        $.post( "/profileprograms/ajaxaddprogramtome", {p_id: id}, function(data){
            $.unblockUI();

            if(data.response.code == 1){
                alertify.success(data.response.result.msg);
                $this.attr('disabled', true);
            }
            else {
                alertify.error(data.response.result.msg);
            }
        });

    });

    body.on('click' , '.addToFavorites', function(){
        var $this = $(this);
        var $parent = $this;
        var id = $(this).data('id');
        var type = $(this).data('type');
        var link = $(this).data('link');

        if($this.find('i').length > 0){
            $this = $this.find('i');
        }

        if($this.hasClass('fa-star-o')) {
            $.post( "/ajax/ajaxaddtofavorites", {id: id,type: type,link: link}, function(data){
                if(data.response.code == 1){
                    alertify.success(data.response.message);
                    $this.removeClass('fa-star-o');
                    $this.addClass('fa-star');
                    $('.favorites-text', $parent).text('В избранном');
                    $parent.addClass('active');
                    /*
                     if($this.parent().find('.in_favorites_count').size() > 0){
                     var $countNow = parseInt($this.parent().find('.in_favorites_count').html());
                     $this.parent().find('.in_favorites_count').text($countNow+1);
                     }
                     else {
                     $('<span class="in_favorites_count">1</span>').insertAfter($this);
                     }
                     */
                }
                else {
                    alertify.error(data.response.error);
                }
            });
        }
        if($this.hasClass('fa-star')) {
            $.post( "/ajax/ajaxdeltofavorites", {id: id,type: type}, function(data){
                if(data.response.code == 1){
                    alertify.warning(data.response.message);
                    $this.removeClass('fa-star');
                    $this.addClass('fa-star-o');

                    $('.favorites-text', $parent).text('Добавить в избранное');
                    $parent.removeClass('active');
                    /*
                     if($this.parent().find('.in_favorites_count').size() > 0){
                     var $countNow = parseInt($this.parent().find('.in_favorites_count').html());
                     if($countNow == 1){
                     $this.parent().find('.in_favorites_count').remove();
                     }
                     else $this.parent().find('.in_favorites_count').text($countNow-1);
                     }
                     */
                }
                else {
                    alertify.error(data.response.error);
                }
            });
        }

        return false;
    });

    body.on('click' , '.delToFavorites', function(){
        var $this = $(this);
        var id = $(this).data('id');
        var type = $(this).data('type');
        var count = parseInt($('#tab_favorites_'+type+' .favorites_count').text());

        $.post( "/ajax/ajaxdeltofavorites", {id: id,type: type}, function(data) {
            if (data.response.code == 1) {
                alertify.success(data.response.message);

                $this.parent().hide('fast', function(){
                    $this.parent().remove();
                    $('#tab_favorites_'+type+' .favorites_count').text(count-1);

                });
            }
            else {
                alertify.error(data.response.error);
            }
        });

        return false;
    });



    body.on('click', '.comment-reply-link', function(){
        var $this = $(this);


        $('#cancel-comment-reply-link').removeClass('hide');
        var commentId = $(this).data('id');
        var form = $('#respond');
        var comment = $('#comment', form);
        comment.val('');
        commentareaeditor.html('');

        $('#comment_parent', form).val(commentId);
        $(form).appendTo("#comments #comment-id-"+commentId);
        comment.focus();
        if($this.hasClass('simple')) {
            if($this.data('author') != ''){
                comment.val($this.data('author')+', ');
                commentareaeditor.html($this.data('author')+', ');
            }
        }

    });



    body.on('keydown', '#commentform', function (e) {

        if (e.ctrlKey && e.keyCode == 13) {
            $('#commentform').submit();
        }
    });

    body.on('keydown', '#editComment',function (e) {

        if (e.ctrlKey && e.keyCode == 13) {
            $('#editComment').submit();
        }
    });

    body.on('focus', '#comment',function (e) {
        $(this).addClass('focused');
    });

    body.on('submit', '#editComment', function () {
        var form = $(this);

        if ($('#comment', form).val() != '') {
            var $data = $(this).serialize();

            $.blockUI({message: '<h2>Подождите...</h2>'});
            $.ajax({
                url: "/ajax/ajaxeditcomment",
                data: $data,
                success: function (data) {
                    $.unblockUI();
                    if (data.response.code == 0) {
                        alertify.error(data.response.error);
                        return false;
                    }
                    if (data.response.code == 1) {
                        $('.comment-actions').show();
                        $(data.response.selector+' .text-of-comment .text-comment').html(data.response.html);
                        $('.text-of-comment .text-comment').show();
                        $('.text-of-comment .edit-comment-block').html('');

                        alertify.success(data.response.message);
                    }
                },
                cache: false, type: "POST", dataType: 'json'
            });

            return false;
        }
        else {
            alertify.warning('Заполните все поля');
        }

        return false;
    });

    body.on('submit', '#commentform', function () {
        var form = $('#commentform');

        if ($('#comment', form).val() != '') {
            var $data = $(this).serialize();
            var parentId = $('#comment_parent', form).val();

            $.blockUI({message: '<h2>Подождите...</h2>'});
            $.post("/ajax/ajaxcomment", $data, function (data) {
                $.unblockUI();

                if (parentId == 0) {
                    $('.commentlist').prepend(data);
                }
                else {
                    $('#comments #comment-id-' + parentId).append('<ul class="children">'+ data +'</ul>');
                }

                $('.comment-body').switchClass( "appending", "", 1000, "easeInOutQuad" );

                $('#comment', form).val('');
                $('.commentarea').redactor('code.set', '');
                respondComplete();

                $('.comments-title').removeClass('hide')
                comCountFunction('plus');

                alertify.success('Комментарий добавлен');
            });

            return false;
        }
        else {
            alertify.warning('Заполните все поля');
        }

        return false;
    });


    $('.openModal').fancybox({
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    $(".phone_mask").mask("+7 (999) 999-9999");

    body.on("click", '.yandexGoal', function(){
        if(!dev) yaCounter33462773.reachGoal($(this).data('target'));
    });

    // Tapping into swal events
    var isMobile = {
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        }
    };
    function onOpen() {
        var offset = document.body.scrollTop;
        document.body.style.top = (offset * -1) + 'px';
        document.body.classList.add('modal--opened');
    }

    function onClose() {
        var offset = parseInt(document.body.style.top, 10);
        document.body.classList.remove('modal--opened');
        document.body.scrollTop = (offset * -1);
    }

    body.on("click", '.authLink', function(){
        if(isMobile.iOS()) {
            onOpen();
        }

        if(hasMainSlider) mainSlider.stopAuto();
        if($(this).hasClass('blockedAuth')){
            alertify.warning('Необходимо авторизоваться на сайте');
        }

        /*Выводим при существовании адапвтиных страниц*/
        if (($("body").hasClass("frontend_articles_index")) || ($("body").hasClass("frontend_articles_show")) || ($("body").hasClass("frontend_news_index")) || ($("body").hasClass("frontend_news_show")) || ($("body").hasClass("frontend_articles_tags")) || ($("body").hasClass("frontend_programs_index")) || ($("body").hasClass("frontend_diets_index")) || ($("body").hasClass("frontend_programs_show")) || ($("body").hasClass("frontend_diets_show")) || ($("body").hasClass("frontend_exercises_show")) || ($("body").hasClass("frontend_clubs_index")) || ($("body").hasClass("frontend_clubs_show"))|| ($("body").hasClass("adaptive-on"))) {
            var w = $(window).width();
            if (w <= '1080') {
                slideoutLeft.close();


                if(document.getElementById('dropdown-user-menu-right')) {
                    slideoutRight.close();
                }
            }
        }

        /*Правим косяк в сафари*/
        $('.authLink').addClass('close-auth-popup1');
        /*Конец*/
        body.toggleClass('show-authorization');
        // $('#mask').toggleClass('display_block');

        $('.auth-popup').toggleClass('show', 950, "easeOutSine" );
        $('.navbar-fixed-top').toggleClass('auth_mod', 1000, "easeOutSine" );
        $('.shop-navbar').toggleClass('auth_mod', 1000, "easeOutSine" );
        $('#username', '#authForm').focus();

        return false;
    });

    body.on("click", '.close-auth-popup', function(){
        /*Правим косяк в сафари*/
        $('.authLink').removeClass('close-auth-popup1');
        if(isMobile.iOS()) {
            onClose();
        }
        /*Конец*/
        $('.auth-popup').toggleClass('show', 950, "easeOutSine" );
        body.removeClass('show-authorization');
        // $('#mask').removeClass('display_block');
        $('.navbar-fixed-top').toggleClass('auth_mod', 1000, "easeOutSine" );
        $('.shop-navbar').toggleClass('auth_mod', 1000, "easeOutSine" );

        return false;
    });

    /*Правим косяк в сафари*/
    body.on("click", '.close-auth-popup1', function(){
        if(isMobile.iOS()) {
            onClose();
        }
        $('.authLink').removeClass('close-auth-popup1');
    });
    /*Конец*/

    if(body.hasClass('need_auth')){
        var content = '<div class="alertify-description">Заполните недостающие данные</div>' +
            '<form class="form" id="afterRegForm">' +
            '<div class="form-group"><label for="username">E-mail</label>' +
            '<input type="text" class="form-control flat" id="username" name="username" placeholder="E-mail"></div>' +
            '<div class="form-group"><label for="password">Пароль</label>' +
            '<input type="password" class="form-control flat" id="password" name="password" placeholder="Пароль"></div>' +
            '<div class="form-group"><label for="retypePassword">Повторите пароль</label>' +
            '<input type="password" class="form-control flat" id="retypePassword" name="retypePassword" placeholder="Повторите пароль"><br><p>Регистрируясь, вы соглашаетесь с <a href="/politics">политикой обработки персональных данных</a></p></div>';

        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Сохранить', cancel:'Отмена'},
                'message': content ,
                'modal': true,
                'pinnable': true,
                onshow:function(){
                    var form = $('#afterRegForm');
                    $('input', form).val('');
                },
                'onok': function(){
                    $.blockUI({message: '<h2>Подождите...</h2>'});
                    var form = $('#afterRegForm');

                    $.post( "/profile/ajaxafterreg", form.serializeArray(), function(data){
                        if(data.response.code == 1) {
                            alertify.success(data.response.message);
                            alertify.closeAll();
                        }
                        else {
                            alertify.error(data.response.error);
                        }

                        $.unblockUI();
                    });

                    return false;
                },
                'oncancel': function(){

                }
            }).show();
    }

    body.on("click", '.loginBtn', function(){
        auth('login-simple');
        return false;
    });
    body.on("click", '.regBtn', function(){
        auth('reg');
        return false;
    });

    body.on("click", '.letsReg', function(){
        var content = '<form class="form" id="regForm">' +
            '<div class="form-group"><label for="username">Логин или e-mail</label>' +
            '<input type="text" class="form-control flat" id="username" name="username" placeholder="Логин или e-mail"></div>' +
            '<div class="form-group"><label for="password">Пароль</label>' +
            '<input type="password" class="form-control flat" id="password" name="password" placeholder="Пароль"><br><p>Регистрируясь, вы соглашаетесь с <a href="/politics">политикой обработки персональных данных</a></p></div>';

        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Зарегистрироваться', cancel:'Отмена'},
                'message': content ,
                'modal': true,
                'pinnable': true,
                onshow:function(){
                    var form = $('#regForm');
                    $('input', form).val('');
                },
                'onok': function(){
                    auth('reg');

                    return false;
                },
                'oncancel': function(){

                }
            }).show();
    });


    var openMessageModal = function(userId){
        alertify.dialog('confirm')
            .set({
                'title' : 'Отправить сообщение',
                'labels':{ok:'Отправить', cancel:'Отмена'},
                'message': '<form id="messageInit">' +
                '<input type="hidden" class="userId" name="to" value=""/>' +
                '<div class="form-group"><textarea id="message-text" name="text" class="form-control"></textarea></div>' +
                '</form>',
                'modal': true,
                'onshow': function(){
                    var form = $('#messageInit');
                    $(".userId", form).val(userId);
                },
                'onok': function(){
                    var form = $('#messageInit');
                    $(".userId", form).val(userId);

                    if($('#message-text', form).val() != ''){
                        $.blockUI({message: '<h2>Подождите...</h2>'});

                        $.post("/dialogs/ajaxinit", form.serializeArray(), function( data ) {
                            if(data.response.success){
                                window.location.replace("/dialogs/d"+data.response.hash);
                                alertify.closeAll();
                            }
                            else
                            {
                                alertify.error(data.response.error);
                            }

                            $.unblockUI();
                        });
                    } else {
                        alertify.error('Заполните обязательное поле');
                        return false;
                    }
                },
                'oncancel': function(){

                }
            }).show();
    };

    body.on('click', '.sendMessage', function(){
        openMessageModal($(this).data("id"));
        return false;
    });

    body.on("click", '.forgotPass', function(){
        var content = '<form class="form" id="formForgot">' +
            '<div class="form-group"><label for="username">Логин или e-mail</label>' +
            '<input type="text" class="form-control flat" id="username" name="username" placeholder="Логин или e-mail"></div>';

        alertifySize();

        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Отправить пароль', cancel:'Отмена'},
                'message': content ,
                'modal': false,
                'pinnable': true,
                onshow:function(){
                    var form = $('#formForgot');
                    $('#username', form).val('');
                },
                'onok': function(){
                    var form = $('#formForgot');
                    var username = $('#username', form).val();
                    if(username != '') {
                        $.blockUI({message: '<h2>Подождите...</h2>'});

                        $.post( "/index/ajaxforgot", {username: username}, function(data){
                            if(data.response.code == 1) {
                                alertify.success(data.response.message);
                                alertify.closeAll();
                            }
                            else {
                                alertify.error(data.response.error);
                            }

                            $.unblockUI();
                        });
                    }
                    else {
                        alertify.warning('Заполните поле');
                    }
                    return false;
                },
                'oncancel': function(){

                }
            }).show();
    });

    function auth($type){
        var form = $('#authForm');
        if($type == 'reg') form = $('#regForm');

        var login = $('#username', form);
        var password = $('#password', form);

        if(login.val() != '' && password.val() != '') {
            $.blockUI({message: '<h2>Подождите...</h2>'});
            var $data = form.serialize();
            $data = $data+'&type='+$type+'&url='+window.location.href;

            $.ajax({
                url: "/index/ajaxsign",
                data: $data,
                success: function (data) {
                    $.unblockUI();
                    if (data.response.code == 0) {
                        alertify.error(data.response.error);
                        return false;
                    }
                    if (data.response.code == 1) {

                        alertify.success(data.response.message);
                        setTimeout( location.reload(), 1000 );
                    }
                },
                cache: false, type: "POST", dataType: 'json'
            });
        }
        else alertify.warning('Введите данные для входа');

    }

    body.on("click", '.userNotice', function(){
        var $this = $(this);
        var $count = parseInt($('.notices-counter').text());

        $.post('/ajax/ajaxsetviewnotice', {id: $this.data('id')}, function(data) {
            var data = data.response;

            if(data.success) {
                var $current = $count-1;
                if($current < 0) $current = 0;
                $('.notices-counter').text($current);
                $this.remove();
                if($current == 0) {
                    $('.userNoticesBlock').remove();
                }
                href = $this.find('a').attr('href');
                window.location = href;
            }
        });
        return false;
    });

    body.on("click", '.callbackForm', function(){
        alertifySize();

        var content = '<form class="form" id="callbackForm">' +
            '<div class="form-group"><label for="name">Имя</label>' +
            '<input type="text" class="form-control flat" id="name" required name="name" placeholder="Имя">' +
            '<label for="phone">Телефон</label>' +
            '<input type="text" class="form-control flat phone_mask" required id="phone" name="phone" placeholder="Телефон">' +
            '</div>';

        alertify.dialog('confirm')
            .set({
                'title' : 'Заказать звонок',
                'labels':{ok:'Заказать звонок', cancel:'Отмена'},
                'message': content ,
                'modal': false,
                'pinnable': true,
                onshow:function(){
                    var form = $('#callbackForm');
                    $('input', form).val('');
                    $(".phone_mask").mask("+7 (999) 999-9999");
                    setTimeout(function(){
                        $('#name', form).focus();
                    }, 1000);
                },
                'onok': function(){
                    var form = $('#callbackForm');
                    var phone = $('#phone', form).val();
                    if(phone != '') {
                        $.blockUI({message: '<h2>Подождите...</h2>'});

                        $.post( "/ajax/ajaxcallback?dev=1", form.serializeArray(), function(data){
                            if(data.response.code == 1) {
                                alertify.success(data.response.message);
                                alertify.closeAll();
                            }
                            else {
                                alertify.error(data.response.error);
                            }

                            $.unblockUI();
                        });
                    }
                    else {
                        alertify.warning('Заполните поле');
                    }
                    return false;
                },
                'oncancel': function(){

                }
            }).show();
    });

    body.on("click", '.more_button', function()
    {
        var pager = $(this).data("page");
        var model = $(this).data("model");
        var params = $(this).data("params");
        var itemCount = $(this).data("itemcount");
        var editable = false;

        if($(this).data("editable") != undefined) {
            editable = $(this).data("editable");
        }

        $('.more_button').hide();
        loading('show', '.block_container', false);

        $.ajax({
            type: "POST",
            url: "/ajax/ajaxloadmore",
            data: {'page' : pager+1, 'itemcount' : itemCount, 'model' : model, 'params' : params, editable: editable},
            cache: false,
            success: function(data){
                $('.more_button_block').remove();
                $(".ajax-content-more").append(data);
                lazyLoad();

                loading('hide');
            }
        });

        return false;
    });

    body.on("click", '.moreComments', function(){
        var $this = $(this);
        loading('show', '.commentlist', false);
        $this.data("page", parseInt($this.data("page")+1));

        $.post("/ajax/ajaxgetcomments", $(this).data(), function(data){
            $('.commentlist').append(data.response.html);
            if(data.response.html == "") {
                $this.remove();
            }
            loading('hide');
        }, "json");

        return false;
    });

    body.on('click', '.getItemCount', function () {
        var $id = $(this).data('id');
        if(!$(this).hasClass('active'))
        {
            $(this).addClass('active');$(this).text('Отмена');
            $('#item_id_'+$id).find('.productAction').addClass('change-count');
            $('#item_id_'+$id).find('.countItems').removeClass('hide');
            $('#item_id_'+$id).find('.buyItem').removeClass('hide');
            $('#item_id_'+$id).find('.buyOneClickBlock').addClass('hide');
        }
        else {
            $(this).removeClass('active');$(this).text('В корзину');
            $('#item_id_'+$id).find('.productAction').removeClass('change-count');
            $('#item_id_'+$id).find('.countItems').addClass('hide');
            $('#item_id_'+$id).find('.buyItem').addClass('hide');
            $('#item_id_'+$id).parent().find('.buyOneClickBlock').removeClass('hide');
        }

    });

    $('.custom-checkbox').radiocheck();
    $('.custom-radio').radiocheck();

    $('.help-block .block-button').on('click', function(){
        if($(this).parent().hasClass('hide-help-block')) {
            $(this).parent().removeClass('hide-help-block');
            $(this).parent().addClass('show-help-block');
        }
        else {
            $(this).parent().addClass('hide-help-block');
            $(this).parent().removeClass('show-help-block');
        }
    });

    body.on('change' , '#filtertraining input', function(){
        loading('show', '.content-filtered', true);
        var form = $('#filtertraining').serializeArray();

        $.post( "/ajax/ajaxfiltertraining", form, function(data){
            if(data != '' && data != 1) {
                $('.content-filtered').html(data);
                lazyLoad();
                $('.content-filter').addClass('hide');$('.more_button').hide();
            }
            if(data == 1) {
                $('.content-filtered').html('');
                $('.content-filter').removeClass('hide');$('.more_button').show();
            }
            loading('hide');
        });

    });

    $('.genderType').on('click', function(){
        $('.genderType').removeClass('active');$('.genderType').removeClass('btn-inverse');
        $(this).addClass('active');$(this).addClass('btn-inverse');
        var $type = $(this).data('type');
        $('.targetBlock .filterBtn').hide();
        $('.targetBlock .'+$type).show();

        $('.targetBlock').removeClass('hide');
    });

    $('.deleteFromCart').on('click', function(){
        var $id = $(this).data('id');
        var $var = $(this).data('variable');

        $.blockUI({message: '<h2>Подождите...</h2>'});
        $.ajax({
            url: "/index/ajaxdelorder",
            data: {id: $id, variable: $var},
            success: function (data) {
                $.unblockUI();
                if (data.response.code == 0) {
                    alertify.success(data.response.error);
                    return false;
                }
                if (data.response.code == 1) {
                    alertify.success(data.response.message);
                    if($var == undefined) $('#item_id_'+$id).remove();
                    else $('#item_id_'+$var).remove();

                    var $price = 0;
                    $('.orderTrPrice b').each(function(){
                        $price = $price + parseInt($(this).text());
                    });

                    $('#sendOrderForm').find('.itemPRICE').val($price);
                    $('#sendOrderForm').find('.price .priceEcho').text($price);

                    if(data.response.redirect) {
                        setTimeout(function () {
                            window.location = '/shop'
                        }, 1000);
                    }
                }
            },
            cache: false, type: "POST", dataType: 'json'
        });
    });

    $('.popoverItem').popover();
    body.on('click', '.backLink', function(){
        window.history.back();
    });


    buyOneClickFunctions();

    function buyOneClickFunctions(){
        $("#sendOneClickForm, #sendOrderForm").validationEngine({
            showPrompts: false,
            showOneMessage: true,
            autoHidePrompt : true,
            ajaxFormValidation: true,
            onBeforeAjaxFormValidation: ajaxValidationOrderForm,
            addSuccessCssClassToField : 'has-success',
            addFailureCssClassToField : 'has-error',
            onValidationComplete: function(form, status){
                if(!status) {
                    alertify.warning('Заполните все поля');
                }
            }

        });

        $('.addingPriceEcho').html('');
        if($('.sendingRadio:checked').val() == 1) $('.addingPriceEcho').html('+ '+$('.sendingRadio:checked').data('price')+' <span class="fa fa-rub"></span> доставка');
        $('.sendingRadio').on('change', function(){
            $('.indexInput').addClass('hide');
            $('.indexInput input').removeClass('validate[required]');

            $('.noteBlock').hide();
            $(this).parent().find('.noteBlock').show();


            if($(this).val() == 2) {
                $('.indexInput').removeClass('hide');
                $('.indexInput input').addClass('validate[required]');
            }

            $('.addingPriceEcho').html('');
            if($(this).val() == 1) $('.addingPriceEcho').html('+ '+$(this).data('price')+' рублей доставка');


        });

        $('#buyOneClickModal').find('.count').on('spinchange', function( event, ui ){
            buyOneClickSpin($(this).val());
        });

        $('#buyOneClickModal').find('.count').on('spin', function( event, ui ){
            buyOneClickSpin(ui.value);
        });

        $('.showViewProduct').find('.spinner').on('spinchange', function( event, ui ){
            showProductSpin($(this), $(this).val());
        });

        $('.showViewProduct').find('.spinner').on('spin', function( event, ui ){
            showProductSpin($(this), ui.value);
        });

        $('#sendOrderForm').find('.count').on('spinchange', function( event, ui ){
            sendOrderFormSpin($(this), $(this).val());
        });

        $('#sendOrderForm').find('.count').on('spin', function( event, ui ){
            sendOrderFormSpin($(this), ui.value);
        });
    }

    $('.targetBlock .resetFilter').on('click', function(){
        $(this).addClass('hide');
        $('.targetBlock .filterBtn').removeClass('active');
        $('.targetBlock .filterBtn').removeClass('btn-inverse');
        $('.genderType').removeClass('active');
        $('.genderType').removeClass('btn-inverse');
        $('.targetBlock').addClass('hide');

        var $category_id = $(this).data('category');
        ajaxFilter($category_id);

    });

    $('.targetBlock .filterBtn').on('click', function(){
        if(!$(this).hasClass('active')) {
            $('.targetBlock .filterBtn').removeClass('active');
            $('.targetBlock .filterBtn').removeClass('btn-inverse');
            $(this).addClass('active');
            $(this).addClass('btn-inverse');

            $('.targetBlock .resetFilter').removeClass('hide');
            var $category_id = $(this).data('category');

            ajaxFilter($category_id);
        }
        return false;
    });

    body.on('click', '.deleteParent', function(){
        $(this).parent().remove();

        return false;
    });

    body.on('click', '.buyItemOneClick', function () {
        $('#buyOneClickModal').modal('show');
        $('#buyOneClickModal').find('.itemName').text('');
        $('#buyOneClickModal').find('.itemID').val('');
        $('#buyOneClickModal').find('.itemPRICE').val('');
        $('#buyOneClickModal').find('.oneItemPrice').val('');
        $('#buyOneClickModal').find('.price .priceEcho').text('');
        $('#buyOneClickModal').find('.count').val(1);

        $('#buyOneClickModal').find('.itemName').text($(this).data('name'));

        $('#buyOneClickModal').find('.itemID').val($(this).data('id'));

        $('#buyOneClickModal').find('.oneItemPrice').val($(this).data('price'));

        $.post('/product/ajaxgetvars', {id: $(this).data('id')}, function(data){
            if(data != 0){
                $('.productVarsBlock').html(data);
                $("#buyOneClickModal .select").select2({dropdownCssClass: 'dropdown-inverse'});

            }

            itemPriceWithVars(true);
        });

    });

    body.on('click', '.buyItemOneClickEvo', function () {
        var $this= $(this);
        alertifySize();
        alertify.dialog('confirm')
            .set({
                'title' : 'Купить в 1 клик',
                'labels':{ok:'Купить', cancel:'Отмена'},
                'message': "<div class='alertifyOneClick'></div>",
                'modal': true,
                'onshow': function(){
                    $.blockUI({message: '<h2>Подождите...</h2>'});
                    loading('show', $('.alertifyOneClick'), true);

                    var container = $('.buyOneClickModalContainer');
                    container.find('.itemPRICE').val('');
                    container.find('.price .priceEcho').text('');
                    container.find('.count').val(1);

                    $.post('/product/ajaxgetoneclick', {id: $this.data('id')}, function(data){
                        if(data != 0){
                            $('.alertifyOneClick').html(data);

                            $( ".spinner" ).spinner({ min: 1 });
                            $(':checkbox').radiocheck();
                            $(':radio').radiocheck();
                            $(".select", '.alertifyOneClick').select2({dropdownCssClass: 'dropdown-inverse'});
                            $(".phone_mask").mask("+7 (999) 999-9999");

                            buyOneClickFunctions();
                        }

                        itemPriceWithVars(true);
                        loading('hide');
                        $.unblockUI();
                        alertifySize('.buyOneClickModalContainer');
                    });
                },
                'onok': function(){
                    $('#sendOneClickForm').submit();
                    return false;
                },
                'oncancel': function(){

                }
            }).show();
    });

    itemPriceWithVars(false);

    body.on('change', '.showViewProduct .imSwitchVar', function(){
        itemPriceWithVars(false);
    });

    body.on('change', '#sendOneClickForm .imSwitchVar', function(){
        itemPriceWithVars(true);
    });

    body.on('click', '.buyItem', function () {
        var $showpage = false;
        if($(this).hasClass('showPageBuy')) $showpage = true;

        var $id = $(this).data('dom');
        var $type = $(this).data('type');

        if($type == undefined) {
            $type = 'simple';
            var $countInput = $(this).parent().find('.countItems').val();
        }
        else {
            var $countInput = 1;
        }



        $.blockUI({message: '<h2>Подождите...</h2>'});
        $.ajax({
            url: "/index/ajaxaddorder",
            data: {type: $type, id: $(this).data('id'), vars: $('#productVars').serializeArray(), count: $countInput, price: $(this).data('price')},
            success: function (data) {
                $.unblockUI();
                if (data.response.code == 0) {
                    alertify.success(data.response.error);
                    return false;
                }
                if (data.response.code == 1) {
                    var $data = data.response.data;
                    var $count = parseInt($data.count);
                    var $price = parseInt($data.price);


                    alertify.success(data.response.message);
                    if($('.minicart-evo').hasClass('empty'))
                    {
                        var $html = '<a href="/order/" title="Перейти в корзину">'+
                            '<span class="cart-counter counter-evo blue-counter">'+$count+'</span>'+
                            '<img class="cart-menu-icon" src="/assets/svg/design/cart.svg"/>'+
                            '</a>';
                        $('.minicart-evo').html($html);
                        $('.minicart-evo').removeClass('empty');
                    }
                    else {
                        var thisCount = parseInt($('.minicart-evo .cart-counter').text());
                        $('.minicart-evo .cart-counter').text(thisCount+$count);
                    }

                    if(!$showpage){
                        $('.productAction').removeClass('change-count');
                        $('#item_id_'+$id).find('.getItemCount').removeClass('active');
                        $('#item_id_'+$id).find('.getItemCount').text('Купить');
                        $('#item_id_'+$id).find('.countItems').addClass('hide');$('#item_id_'+$id).find('.countItems').val(1);
                        $('#item_id_'+$id).find('.buyItem').addClass('hide');
                        $('#item_id_'+$id).parent().find('.buyOneClickBlock').removeClass('hide');
                        itemPriceWithVars(false);
                    }
                    else {
                        $('.showViewProduct .additionalPrice').html('');
                        if($type == 'simple') $('.countItems').val(1);
                        //if($type == 'course') $('#item_id_'+$id).find('.buyItem').remove();
                    }



                }
            },
            cache: false, type: "POST", dataType: 'json'
        });


        alertify.dialog('confirm')
            .setting({
                'labels':{ok:'Сделать заказ!', cancel:'Продолжить выбор'},
                'message': 'Вы добавили товар в корзину!' ,
                'onshow': function(){
                    alertifySize();
                },
                'onok': function(){
                    setTimeout(function () {
                        window.location = '/order'
                    }, 1000);
                },
                'oncancel': function(){}
            }).show();

        return false;
    });


    body.on('click', '.openCoachBlock', function(){
        var $this = $(this);
        var parent = $this.closest('.individual-coach');
        parent.addClass('active');
        $this.hide();
        $('.coachIndividualLink', parent).hide();
        $('.individual-coach-close', parent).show();
        body.append('<div class="evo-overlay"></div>');
        $('.individual-coach-hided', parent).fadeIn( "fast", function() {
            matchHeight();
        });

        return false;
    });
    body.on('click', '.evo-overlay', function(){
        $(this).remove();
        if($('.individual-coach').size() > 0) {
            $('.closeCoachBlock').trigger('click');
        }
    });

    body.on('click', '.closeCoachBlock', function(){
        var $this = $(this);
        var parent = $this.closest('.individual-coach');
        parent.removeClass('active');
        $('.evo-overlay').remove();
        $('.coachIndividualLink', parent).show();
        $('.individual-coach-close', parent).hide();
        $('.individual-coach-hided', parent).fadeOut( "fast", function() {
            $('.openCoachBlock', parent).show();
            matchHeight();
        });

        return false;
    });

    $("html").click(function() {
        $(".closeoutside").hide();
    });

    body.on("keyup", ".mindSearch", function(){
        var $this = $(this);
        var query = $this.val();

        if(query.length > 2) {
            $.post("/ajax/ajaxsearch", {query: query}, function(data){
                var html = "";

                $.each(data.hits, function(type, items){
                    html = html + "<div class='mind-search-result-type'><div class='mind-search-result-type-title'>"+type+"</div>";
                    $.each(items, function(index, item){
                        html = html + "<div class='mind-search-result-item clr'>" +
                            "<a href='/"+item.url+"'><div class='mind-search-result-item-img'>"+(item.thumbnail != "" ? "<img src='"+item.img+"' />" : "")+"</div>" +
                            "<div class='mind-search-result-item-content'>"+item.title+"</div>" +
                            "</a></div>";
                    });
                    html = html + "</div>";
                });

                if(html != "") {
                    $(".mindSearchResult").html(html).show();
                    //initScroll(".mindSearchResult", "y", true);
                }
                else {
                    $(".mindSearchResult").html("").hide();
                }


            }, "json");
        }
        else {
            $(".mindSearchResult").html("").hide();
        }
    });

    $("#sendContactForm").validationEngine({
        showPrompts: false,
        showOneMessage: true,
        autoHidePrompt : true,
        ajaxFormValidation: true,
        onBeforeAjaxFormValidation: ajaxValidationContactForm,
        addSuccessCssClassToField : 'has-success',
        addFailureCssClassToField : 'has-error',
        onValidationComplete: function(form, status){
            if(!status) {

            }
        }

    });

    $("#sendHelpForm").validationEngine({
        scroll: false,
        showPrompts: false,
        showOneMessage: true,
        autoHidePrompt : true,
        ajaxFormValidation: true,
        onBeforeAjaxFormValidation: ajaxValidationHelpForm,
        addSuccessCssClassToField : 'has-success',
        addFailureCssClassToField : 'has-error',
        onValidationComplete: function(form, status){
            if(!status) {

            }
        }

    });
});

$(window).on('load', function () {
    $('body').removeClass('no_animation');
});

function setupCommentsEditor(){
    var commentarea = $('.commentarea').redactor({
        focus: false,
        //lang: 'ru',
        plugins: ["video", "imagemanager", "clips"],
        buttons: ['image', 'video', 'link'],
        imageUpload: '/ajax/ajaxredactoruploader',
        pasteBeforeCallback: function(html)
        {
            if(html.search("img") == -1){
                return html;
            }

            if($(html).hasClass('emoji')){
                $(html).attr('style', "");
                this.insert.htmlWithoutClean(html);
            }
            else {
                return html;
            }
        }
    });

    var commentareaeditor = $('#respond .redactor-editor');
    commentareaeditor.html('');
}

function setupAjaxTabs(){
    $("body").on("click", ".force-ajax-tab", function(){
        $(".ajax-tab[href='"+$(this).attr("href")+"']").click();

        return false;
    });
    $("body").on("click", ".ajax-tab", function(){
        var $this = $(this);
        var selector = $this.attr("href");
        var parent = $this.closest(".ajax-tab-content");
        var activeTab = $(selector, parent);

        $(".ajax-tab", parent).removeClass("active");
        $this.addClass("active");

        $(".ajax-tab-pane", parent).removeClass("active");
        activeTab.addClass("active");

        $(".ajax-tab-pane", parent).hide();
        activeTab.show();

        if(!activeTab.hasClass("loaded") && $this.data("ajax") != ""){
            loadingEvo('show', $(".ajax-tab-loader", activeTab), true);

            $.post($this.data("ajax"), $this.data(), function(data){
                if(data.response.success) {
                    $(".ajax-tab-loader", activeTab).html(data.response.html);
                    $(".ajax-tab-loader-hided", activeTab).removeClass("hide");

                    $('[data-toggle="tooltip"]').tooltip();
                }
                activeTab.addClass("loaded");
                loadingEvo("hide");
            }, "json");
        }

        return false;
    });

    $(".ajax-tab-content").each(function(){
        var $this = $(this);

        $(".ajax-tab", $this).removeClass("active");
        $(".ajax-tab", $this).first().addClass("active").click();

        $(".ajax-tab-pane", $this).removeClass("active");
        $(".ajax-tab-pane", $this).first().addClass("active");

        $(".ajax-tab-pane", $this).hide();
        $(".ajax-tab-pane", $this).first().show();
    });
}

function setupBasicUpload(){
    if($('.file_basic_upload').size() > 0)
    {
        $('.file_basic_upload').each(function() {
            var $this = $(this);
            var $file_one = [];
            $file_one['selector'] = $this;
            $file_one['input'] = $this.data('input');
            $file_one['script'] = $this.data('script');
            $file_one['single'] = $this.data('single');
            $file_one['type'] = $this.data('type');
            $file_one['instance'] = $this.data('instance');

            var parent = $this.closest(".file_basic_upload_block");

            $this.fileupload({
                url: $file_one['script'],
                type: 'post',
                singleFileUploads: true,
                formData: {instance: $file_one['instance']},
                dataType: 'json',
                start: function (e, data) {
                    $.blockUI({message: '<h2>Подождите...</h2>'});
                },
                success: function (data) {
                    console.log(data);
                    if($file_one['type'] == "basic") {
                        var $html = '<input type="hidden" name="' + $file_one['input'] +'"  value="' + data.fileName+'"/><img width="155" height="155" class="img-thumbnail" src="'+data.domain + '/' + data.path + '/' + data.fileName+'" />';

                        if(parent.length) {
                            $('.this-image', parent).html($html);
                        }
                        else {
                            $('.this-image').html($html);
                        }
                    }

                    if($file_one['type'] == "basic-multi") {
                        var $html = '<div class="relative" style="width: 155px;">' +
                            '<input type="hidden" name="' + $file_one['input'] +'"  value="' + data.fileName+'"/>' +
                            '<img width="155" height="155" class="img-thumbnail" src="'+data.domain + '/' + data.path + '/' + data.fileName+'" />' +
                            '<a href="#" style="position: absolute; top: 0; left: 0;" class="deleteParent btn btn-danger-fill btn-sm glyphicon glyphicon-trash"></a>' +
                        '</div>';

                        if(parent.length) {
                            $('.this-image', parent).append($html);
                        }
                        else {
                            $('.this-image').append($html);
                        }
                    }

                },
                done: function (e, data) {
                    $.unblockUI();
                },
                progressall: function (e, data) {
                    //var progress = parseInt(data.loaded / data.total * 100, 10);
                }
            });
        });
    }
}

function initRatingSlider(){
    if($(".rating_slider").size() > 0) {
        $(".rating_slider").slider({
            min: 0,
            max: 10,
            value: $(".rating_slider").data('rating'),
            step: 0.1,
            orientation: "horizontal",
            range: "min",
            create: function (event, ui) {
                if ($(this).data('isvote') == 1) {
                    $('.rating_slider').slider("option", "disabled", true);
                }
            },
            slide: function (event, ui) {
                $(".rating_slider_amount").text(ui.value);
            },
            change: function (event, ui) {
                var $this = $(this);
                alertify.dialog('confirm')
                    .setting({
                        'labels': {ok: 'Проголосовать!', cancel: 'Отмена'},
                        'message': 'Рейтинг изменится после того как вы проголосуете!',
                        'onok': function () {
                            ajaxRating(ui.value, $this.data('post_id'), $this.data('post_type'));
                        },
                        'oncancel': function () {
                        }
                    }).show();
            }
        });
    }
    $(".rating_slider_amount").text($(".rating_slider").data('rating'));
}

function ajaxRating($rating, $post_id, $post_type){
    $('.rating_slider').slider( "option", "disabled", true );
    $.ajax({
        url: "/index/ajaxsetrating",
        data: {'post_id' : $post_id, 'post_type' : $post_type, 'rating' : $rating},
        success: function (data) {
            if (data.response.code == 0) {
                alertify.error(data.response.message);
                $('.rating_slider').slider( "option", "disabled", false );
                return false;
            }
            if (data.response.code == 1) {
                var now = parseFloat($('.rating_slider_amount').text());
                var total = parseFloat(data.response.total);

                var $el = $('.rating_slider_amount');
                var $thisCount = $('.this_count');

                $({percentage: now}).stop(true).animate({percentage: total}, {
                    duration : 2000,
                    easing: "easeOutExpo",
                    step: function () {
                        var percentageVal = Math.round(this.percentage * 10) / 10;
                        $el.text(percentageVal);
                    }
                }).promise().done(function () {
                    $el.text(total);
                    $thisCount.text(parseInt($thisCount.text()) + 1);
                    $('.rating-info').prepend('<div class="rating_notice"><span class="glyphicon glyphicon-ok"></span> Вы уже проголосовали!</div></div>');
                    $('.rating_slider').hide();
                });

                alertify.success(data.response.message);
            }
        },
        cache: false, type: "POST", dataType: 'json'
    });
}

function sendOrderFormSpin($this, uivalue){
    var $id = $this.data('id');
    var $var = $this.data('variable');
    var $price = $this.data('price');

    var resultPrice = $price*uivalue;

    if($var == undefined) $('#item_id_'+$id).find('.orderTrPrice b').html(resultPrice+' <span class="fa fa-rub"></span>');
    else $('#item_id_'+$var).find('.orderTrPrice b').html(resultPrice+' <span class="fa fa-rub"></span>');

    var $price = 0;


    $('.orderTrPrice b').each(function(){
        $price = $price + parseInt($(this).text());
    });

    $('#sendOrderForm').find('.itemPRICE').val($price);
    $('#sendOrderForm').find('.price .priceEcho').text($price);
}


function itemPriceWithVars(modal, vars){
    var price = 0;

    if(modal) {
        var one_price = parseFloat($('#buyOneClickModal').find('.oneItemPrice').val());

        $("option:selected", '#sendOneClickForm .imSwitchVar').each(function(){
            price = price + parseFloat($(this).data('price'));
        });

        var $count = $('#buyOneClickModal').find('.count').val();

        $('#buyOneClickModal').find('.price .priceEcho').text((price+one_price)*$count);
        $('#buyOneClickModal').find('.itemPRICE').val(price*$count);
    }
    else {
        var one_price = parseFloat($('.itemChangePrice').data('original'));

        $("option:selected", '.showViewProduct .imSwitchVar').each(function(){
            price = price + parseFloat($(this).data('price'));
        });

        $('.itemChangePrice').text((one_price+price));
    }

    return one_price+price;
}

function buyOneClickSpin(uivalue){
    var $price = itemPriceWithVars(true);
    $price = $price*uivalue;

    $('#buyOneClickModal').find('.price .priceEcho').text($price);
    $('#buyOneClickModal').find('.itemPRICE').val($price);

    if(uivalue > 1) $('#buyOneClickModal .additionalPrice').html('<br>= '+$price+' <span class="fa fa-rub"></span>');
    else $('#buyOneClickModal .additionalPrice').html('');
}

function showProductSpin($this, uivalue){
    var $price = itemPriceWithVars(false);
    var $resultPrice = $price*uivalue;
    if(uivalue > 1) $('.showViewProduct .additionalPrice').html('<br>= '+$resultPrice+' <span class="fa fa-rub"></span>');
    else $('.showViewProduct .additionalPrice').html('');
}

function ajaxFilter($category_id){
    loading('show', '.resultAjax', true);
    $.blockUI({message: '<h2>Подождите...</h2>'});
    $.post("/product/ajaxfilter", {id: $category_id}, function (data) {
        $.unblockUI();
        if (data == 0) {
            alertify.error('Произошла ошибка');
            return false;
        }
        else {
            $('.resultAjax').html('');
            $('.resultAjax').html(data);
            $(".spinner").spinner({min: 1});
            lazyLoad();
        }
        loading('hide');
    });
}

function ajaxValidationOrderForm(form, options) {
    var $data = form.serialize();
    $.blockUI({message: '<h2>Подождите...</h2>'});
    $.ajax({
        url: "/index/ajaxorder",
        data: $data,
        success: function (data) {
            $.unblockUI();
            if (data.response.code == 0) {
                alertify.error(data.response.error);
                return false;
            }
            if (data.response.code == 1) {
                $('textarea', form).val('');
                $('input', form).val('');
                alertify.success(data.response.message);
            }
            $('#buyOneClickModal').modal('hide');

            if(data.response.redirect) {
                setTimeout(function () {
                    window.location = data.response.redirect_url
                }, 1000);
            }
            else {
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }

        },
        cache: false, type: "POST", dataType: 'json'
    });

    return false;
}

function loading($status, $where, $prepend){
    var html = '<div class="loading-evo-container">' +
        '<img class="loading-evo-logo svg-image" height="43" src="/assets/svg/design/logo.svg"> ' +
        '<div class="loading-evo"> ' +
        '<div class="indeterminate"></div> ' +
        '</div> ' +
        '</div>';

    if($status == 'show') {
        if($prepend) $($where).prepend(html);
        else $($where).append(html);
        generateSvg();
    }
    if($status == 'hide') $('.loading-evo-container').remove();
}

function loadingEvo($status, $el, $prepend){
    var html = '<div class="loading-evo-container">' +
        '<img class="loading-evo-logo svg-image" height="43" src="/assets/svg/design/logo.svg"> ' +
        '<div class="loading-evo"> ' +
        '<div class="indeterminate"></div> ' +
        '</div> ' +
        '</div>';

    if($status == 'show') {
        if($prepend) $el.prepend(html);
        else $el.append(html);
        generateSvg();
    }
    if($status == 'hide') $('.loading-evo-container').remove();
}

function ajaxValidationContactForm(form, options) {
    var $data = form.serialize();
    $.blockUI({message: '<h2>Подождите...</h2>'});
    $.ajax({
        url: "/index/ajaxform",
        data: $data,
        success: function (data) {
            $.unblockUI();
            if (data.response.code == 0) {
                alertify.error(data.response.error);
                return false;
            }
            if (data.response.code == 1) {
                $('textarea', form).val('');
                $('input', form).val('');
                alertify.success(data.response.message);
            }
        },
        cache: false, type: "POST", dataType: 'json'
    });
    return false;
}

function ajaxValidationHelpForm(form, options) {
    var $data = form.serialize();
    $.blockUI({message: '<h2>Подождите...</h2>'});
    $.ajax({
        url: "/index/ajaxformsend",
        data: $data,
        success: function (data) {
            $.unblockUI();
            if (data.response.code == 0) {
                alertify.error(data.response.error);
                return false;
            }
            if (data.response.code == 1) {
                $('.help-block').removeClass('show-help-block');
                $('.help-block').addClass('hide-help-block');

                $('textarea', form).val('');
                $('input', form).val('');

                $('textarea', form).removeClass('has-success');
                $('input', form).removeClass('has-success');

                alertify.success(data.response.message);
            }
        },
        cache: false, type: "POST", dataType: 'json'
    });
    return false;
}

function changeToFormElement(parentRow){
    $('.toFormInput', parentRow).each(function(){
        var input;
        if($(this).data('type') == 'textarea'){
            input = '<textarea class="form-control" name="'+$(this).data('name')+'">'+$(this).text()+'</textarea>';
        }
        if($(this).data('type') == 'number'){
            input = '<input class="form-control onlyNumber" type="text" name="'+$(this).data('name')+'" value="'+$(this).text()+'"/>';
        }
        if($(this).data('type') == 'input') {
            input = '<input class="form-control" type="text" name="'+$(this).data('name')+'" value="'+$(this).text()+'"/>';
        }
        $(this).html(input);
        $('.onlyNumber').ForceNumericOnly();
    });
}

function declination(a, b, c, s) {

    var words = [a, b, c];
    var index = s % 100;

    if (index >=11 && index <= 14) { index = 0; }
    else { index = (index %= 10) < 5 ? (index > 2 ? 2 : index): 0; }

    return(words[index]);

}

$(window).scroll(function () {
    if ($(this).scrollTop() > 600) {
        $('.scrollup').show();
    } else {
        $('.scrollup').hide();
    }
});
$('.scrollup').click(function () {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    return false;
});

function loadExResults(){
    $(".ajaxExResultCounter").each(function(){
        var $this = $(this);
        var parent = $this.closest(".addExResultContainer");

        $.post("/ajax/ajaxgetresults", $this.data(), function(data){
            $this.html(data.response.html);
            if($(".ajaxExResultPeoples", parent).size() > 0) {
                $(".ajaxExResultPeoples", parent).html(data.response.peoples);
            }

            $('[data-toggle="tooltip"]', parent).tooltip();
        }, "json");
    });


}

function tabContent(){
    $('.tabContent').each(function(i){
        var week = i+1;
        var firstDay = $(this).find('.day-in-week:not(.disabled) a').attr('href');
        if(firstDay != undefined) {
            $('.tab-content '+firstDay).parent().find('.tab-pane').removeClass('active');
            $('.tab-content '+firstDay).addClass('active');
        }
        else {
            $('.tab-content #day_1_week_'+week).parent().find('.tab-pane').removeClass('active');
            $('.tab-content #day_1_week_'+week).addClass('active');
        }

    });

    body.on('click', '.tabContent li a', function(){
        $(window).scrollTop($(window).scrollTop()+1);

        if(!$(this).parent().hasClass('disabled')){
            var togo = $(this).attr('href');
            $('.tab-content '+togo).parent().find('.tab-pane').removeClass('active');
            $('.tab-content '+togo).addClass('active');
            $('.day-in-week a').removeClass('active');
            $(this).addClass('active');
        }

        return false;
    });


}

function ajaxDietscalendar(){
    if($(".dietCalendarLoader").size() > 0) {
        $(".dietCalendarLoader").each(function(e){
            var $this = $(this);

            loadingEvo("show", $this);

            $.post("/ajax/ajaxloaddietcalendar", $this.data(), function(data){
                $this.html(data.response.html);
                tabContent();
                loadingEvo("hide", $this);
            }, "json");
        });
    }
}

function ajaxProgramscalendar(){
    if($(".programCalendarLoader").size() > 0) {
        $(".programCalendarLoader").each(function(e){
            var $this = $(this);

            loadingEvo("show", $this);

            $.post("/ajax/ajaxloadprogramcalendar", $this.data(), function(data){
                $this.html(data.response.html);
                tabContent();
                loadExResults();
                loadingEvo("hide", $this);
            }, "json");
        });
    }
}

function ajaxComments(){
    if($(".ajaxCommentsLoader").size() > 0) {
        $(".ajaxCommentsLoader").each(function(e){
            var $this = $(this);

            loadingEvo("show", $this);

            $.post("/ajax/ajaxloadallcomments", $this.data(), function(data){
                $this.html(data.response.html);
                setupCommentsEditor();
                loadingEvo("hide", $this);
            }, "json");
        });
    }
}

function lazyLoad(){
    var $img = $('img.thumbnail_image');

    $img.each(function(e) {

        var $this = $(this);

        if(!$this.data('original') && !$this.hasClass('lazy')) {
            var $src = $this.attr('src');
            //$this.attr('src', '/images/blank.gif');
            $this.addClass('lazy');
            $this.data('original', $src);
            $this.attr('data-original', $src);

        }
    });

    $("img.lazy").lazyload({
        effect : "fadeIn",
        threshold : 200,
        failure_limit : 3
    });
}

function alertifySize(element) {
    var $alertifyModal = $('.alertify .ajs-dialog');
    var $alertifyModalBody = $('.alertify .ajs-body');
    var alertifyPaddingWidth = 88;
    var alertifyPaddingHeight = 56;


    if(element === undefined){
        $alertifyModal.css('max-width', '500px');
        $alertifyModalBody.css('height', 'auto');
    }
    else {
        $alertifyModal.css('max-width', $(element).width()+alertifyPaddingWidth+'px');

        var height = 300;
        if($(element).height() < 300) {
            height = $(element).height();
            $alertifyModalBody.mCustomScrollbar("destroy");
        }
        else {
            $alertifyModalBody.mCustomScrollbar({
                axis:"y",
                theme:"inset-2-dark",
                autoHideScrollbar: true,
                mouseWheel: { enable: true, axis: "y", scrollAmount: 200 }
            });
        }

        $alertifyModalBody.css('height', height+alertifyPaddingHeight+'px');
    }
}

function openEmitateModal(id, top) {
    var $modal = $('#'+id);
    $modal.show();
    if(top != undefined) $modal.css("top", top);

    $('.emitate-dialog-close', $modal).click(function(){
        $modal.hide();
    });

    body.keyup(function(e) {
        if (e.keyCode == 27) $modal.hide();
    });

    initScroll('.emitate-dialog-content', 'y', true);
}

function initScroll(element, axis, autoHideScrollbar){
    $(element+":not(.scrolling)").mCustomScrollbar({
        axis: axis,
        //setTop: $(document).height()+'px',
        theme:"inset-2-dark",
        autoHideScrollbar: autoHideScrollbar,
        mouseWheel: { enable: true, axis: axis, scrollAmount: 200 }
    });

    $(element).addClass('scrolling');
}


function closeEmitateModal(id) {
    var $modal = $('#'+id);
    $modal.hide();
}

function dropdownsPosition() {
    var noticeBlock = $('.userNoticesBlock');
    if(noticeBlock.length) {
        var noticeDropdown = $('.dropdown-menu', noticeBlock);
        noticeDropdown.css('left', -noticeDropdown.width()+75);
    }

    var messageBlock = $('.userMessagesBlock');
    if(messageBlock.length) {
        var messageDropdown = $('.dropdown-menu', messageBlock);
        messageDropdown.css('left', -messageDropdown.width()+50);
    }
}

function generateSvg(){
    $('img.svg-image').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        var imgHeight = $img.attr('height');
        var imgWidth = $img.attr('width');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            if(typeof imgHeight !== 'undefined') {
                $svg.attr('height', imgHeight);
            }

            if(typeof imgWidth !== 'undefined') {
                $svg.attr('width', imgWidth);
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}

function matchHeight(){
    $('.matchHeight').each(function(){
        var $this = $(this);
        var height = $this.show().height();

        $('.matchHeightMe', $this).css('height', height+'px');
    });
}

jQuery.fn.ForceNumericOnly =
    function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // Разрешаем backspace, tab, delete, стрелки, обычные цифры и цифры на дополнительной клавиатуре
                return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
            });
        });
    };





