$(document).ready(function(){

	$('.rate-control').each(function() {
	    var $this = $(this);
	    $this.raty({
	      starType: 'i',
	      score: function() {
	        return $(this).attr('data-rate');
	      }, readOnly: function() {
	        return $(this).attr('data-readonly') != undefined;
	      }, scoreName: $this.data('name')
	    })
	});
	

	$(".intro-slide").slick({
		slidesToShow: 4,
	  	slidesToScroll: 4,
	  	infinite: true,
	  	arrows: false,
		dots: true,
	  	responsive: [
	    {
	      breakpoint: 1200,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2	        
	      }
	    },	    
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }]
	});


	$(".ajaxExResultCounter").click(function(){
		var resultList = $(this).find(".minimodal-evo").html();
		$(".modal-results-list").html(resultList);

		$(".modal-results-list .closeMinimodal").remove();		

		$(".modal-results-list a").each(function(i){
			var title = $(this).data("original-title");
			$(this).attr("title", title);
			$(this).removeAttr("data-original-title");
		});


		$("#modalResult").modal("show");
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	
	/*$(".intro-slider-md").slick({
		arrows: false,
		dots: true
	});

	$(".intro-slider-sm").slick({
		arrows: false,
		dots: true
	});*/

	$(".b-progress-list").slick({
		slidesToShow: 5,
	  	slidesToScroll: 5,
	  	infinite: true,
	  	responsive: [
	    {
	      breakpoint: 1200,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 4	        
	      }
	    },
	    {
	      breakpoint: 992,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }]
	});


	$(".splash-articles").slick({
		slidesToShow: 4,
	  	slidesToScroll: 4,
	  	infinite: true,
	  	arrows: false,
	  	responsive: [	   
	    {
	      breakpoint: 992,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }]
	});

});