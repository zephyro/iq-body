(function($)
{
	$.Redactor.prototype.clips = function()
	{
		return {
			smileDir:"/js/redactor/plugins-10/smiles/",
			init: function()
			{
				var items = [
					[':smile:'],[':laughing:'],[':blush:'],[':rage:'],[':relaxed:'],[':flushed:'],[':stuck_out_tongue_winking_eye:'],[':stuck_out_tongue:'],[':unamused:'],[':pensive:'],[':persevere:'],[':scream:'],
                    [':triumph:'],[':dizzy_face:'],[':innocent:'],[':heart:'],[':fire:'],[':thumbsup:'],[':fist:'],[':-1:'],[':facepunch:'],[':muscle:'],[':smirk:'],[':stuck_out_tongue_closed_eyes:'],
                    [':sleeping:'],[':grimacing:'],[':cry:'],[':crown:'],[':hankey:'],[':syringe:'],[':pill:'],[':hamburger:'],[':ru:']
				];

				this.clips.template = $('<ul id="redactor-modal-list" class="emoji-mod">');

				for (var i = 0; i < items.length; i++)
				{
                    var alias = items[i][0].replace(/:/g, "");
					var li = $('<li>');
					var a = $('<a href="#" class="redactor-clip-link">').html('<img width="20" height="20" class="redactor-emoji" src="'+this.clips.smileDir+alias+'.png" />');
					
					var smile = '<img class="emoji" src="'+this.clips.smileDir+alias+'.png" />';
					var div = $('<div class="redactor-clip">').hide().html(smile);

					li.append(a);
					li.append(div);
					this.clips.template.append(li);
				}

				this.modal.addTemplate('clips', '<section>' + this.utils.getOuterHtml(this.clips.template) + '</section>');
				
				var button = this.button.add('clips', 'Смайлы');
				this.button.setAwesome('clips', 'fa-smile-o');
				this.button.addCallback(button, this.clips.show);

			},
			show: function()
			{
				this.modal.load('clips', 'Смайлы', 400);

				this.modal.createCancelButton();

				$('#redactor-modal-list').find('.redactor-clip-link').each($.proxy(this.clips.load, this));

				this.selection.save();
				this.modal.show();
			},
			load: function(i,s)
			{
				$(s).on('click', $.proxy(function(e)
				{
					e.preventDefault();
					this.clips.insert($(s).next().html());

				}, this));
			},
			insert: function(html)
			{
				this.selection.restore();
				this.insert.node($(html));
				this.modal.close();
				this.observe.load();
				this.code.sync();
			}
		};
	};
})(jQuery);

