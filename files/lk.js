var body;

$(document).ready(function ($) {
    setupUpload();
    body = $('body');

    if(window.location.hash != ""){
        setTimeout(function() {
            $(".ajax-tab[href='"+window.location.hash+"']").click();
        }, 1000);
    }

    $('#settingsForm').validationEngine({
        showPrompts: false,
        scroll: false,
        showOneMessage: true,
        autoHidePrompt : true,
        ajaxFormValidation: false,
        addSuccessCssClassToField : 'has-success',
        addFailureCssClassToField : 'has-error',
        onValidationComplete: function(form, status){
            if(status) {
                var username = $('#username', form).val();

                $.post( "/profile/ajaxcheckusername", {username: username}, function(data){
                    var data = data.response;

                    if(data.code == 1){
                        form.validationEngine('detach');
                        form.submit();
                        return true;
                    }
                    else {
                        alertify.error(data.error);
                    }
                });
            }
        }
    });

    $('#settingsPasswordForm').validationEngine({
        showPrompts: false,
        scroll: false,
        showOneMessage: true,
        autoHidePrompt : true,
        ajaxFormValidation: true,
        onBeforeAjaxFormValidation: ajaxChangePassword,
        addSuccessCssClassToField : 'has-success',
        addFailureCssClassToField : 'has-error',
        onValidationComplete: function(form, status){
            var password = $('#password', form);
            var retypepassword = $('#retypePassword', form);

            if(status && password.val()!='' && retypepassword.val() !='' && password.val() == retypepassword.val()) {
                return true;
            }
            else {
                retypepassword.removeClass('has-success');
                retypepassword.addClass('has-error');
            }
        }

    });

    body.on('click', '.deleteHistory', function(){
        $('.ajax-lk-progress').html("");
        $('.lk-main-screen-progress-ajax').html("");

        loading("show", ".ajax-lk-progress");
        loading("show", ".lk-main-screen-progress-ajax");

        $.post( "/profile/ajaxdeletehistory", {id: $(this).data('id')}, function(data){
            $('.ajax-lk-progress').html(data.response.html);
            $('.lk-main-screen-progress-ajax').html(data.response.htmlshort);

            loading("hide");
        });
    });

    var $block = '<a class="fb" href="/i/menswomenlinesbtn.png"><img width="415" src="/i/menswomenlinesbtn.png"></a>';
    body.on('click', '.how_it_work_params a', function(){
        alertify.dialog('confirm')
            .set({
                'labels': {ok: 'Читать подробнее', cancel: 'Закрыть'},
                'message': $block,
                'modal': false,
                'pinnable': false,
                'onshow': function () {

                },
                'onok': function () {
                    window.open("/articles/organizm-cheloveka/antropometriua",'_blank');
                    alertify.closeAll();
                }
            }).unpin().show();
    });

    body.on('click', '.how_it_work_params_div', function(){
        var $this = $(this);
        if(!$this.hasClass('opened'))
        {
            $this.after().append($block);
            $this.addClass('opened');
        }
    });

    body.on("click", ".addTarget", function(){
        var $this = $(this);
        var form = $(".addTargetForm");
        if($this.hasClass("active")) {
            form.submit();
        }
        else {
            form.show();
        }

        $this.toggleClass("active");

        return false;
    });

    body.on("submit", ".addTargetForm", function(){
        var $this = $(this);
        var error = false;

        $(".required", $this).each(function () {
            if($(this).val() == "") {
                error = true;
            }
        });

        if(!error) {
            $.blockUI({message: '<h2>Подождите...</h2>'});
            loading("show", ".ajax-lk-targets")
            $.post("/profile/ajaxaddtarget", $this.serializeArray(),function( data ) {
                $.unblockUI();
                loading("hide");

                if(data.response.code == 1){
                    $(".ajax-lk-targets").html(data.response.html);
                    $(".addTargetForm").hide();
                    $(".addTargetForm input").val("");
                    $(".addTarget").removeClass("active");
                }
                else
                {
                    alertify.error(data.response.error);
                }

            }, "json");
        }
        else {
            alertify.error("Заполните все поля");
        }


        return false;
    });

    var contentAddProgram = $('#add-program-modal');
    var addProgramModal = function(){
        alertify.dialog('confirm')
            .set({
                'title' : contentAddProgram.attr('title'),
                'labels':{ok:'Добавить', cancel:'Отмена'},
                'message': contentAddProgram.html() ,
                'modal': true,
                'onshow': function(){
                    contentAddProgram.remove();
                },
                'onok': function(){
                    var form = $('#programForm');

                    if($('#title', form).val() != ''){
                        $.blockUI({message: '<h2>Подождите...</h2>'});

                        $.post("/profileprograms/ajaxsaveform", form.serializeArray(),function( data ) {
                            $.unblockUI();

                            if(data.response.code == 1){
                                window.location.replace("/profile/programs/edit/"+data.response.id);
                                alertify.closeAll();
                            }

                            else
                                alertify.error(data.response.error);
                        });
                    } else {
                        alertify.error('Заполните обязательное поле');
                        return false;
                    }
                },
                'oncancel': function(){

                }
            }).show();
    }

    var contentAddDiet = $('#add-dietprogram-modal');

    var addDietModal = function(item, contentAddProgram){
        alertify.dialog('confirm')
            .set({
                'title' : contentAddProgram.attr('title'),
                'labels':{ok:'Добавить', cancel:'Отмена'},
                'message': contentAddProgram.html() ,
                'modal': true,
                'onshow': function(){
                    contentAddProgram.remove();
                    if(item) {
                        console.log(item);
                    }
                },
                'onok': function(){
                    var form = $('#dietForm');

                    if($('#title', form).val() != ''){
                        $.blockUI({message: '<h2>Подождите...</h2>'});

                        $.post("/profilediets/ajaxsaveform", form.serializeArray(),function( data ) {
                            if(data.response.code == 1){
                                window.location.replace("/profile/diets/edit/"+data.response.id);
                                alertify.closeAll();
                            }
                            else
                            {
                                alertify.error(data.response.error);
                            }

                            $.unblockUI();
                        });
                    } else {
                        alertify.error('Заполните обязательное поле');
                        return false;
                    }
                },
                'oncancel': function(){

                }
            }).show();
    }

    body.on('click', '.addDiet', function(){
        addDietModal(false, contentAddDiet);
    });

    body.on('click', '.addProgram', function(){
        addProgramModal();
    });

    var contentAddProgramWork = $('#add-program-to-work-modal');
    var addProgramWorkModal = function(){
        alertify.dialog('confirm')
            .set({
                'title' : contentAddProgramWork.attr('title'),
                'labels':{ok:'Добавить', cancel:'Отмена'},
                'message': contentAddProgramWork.html() ,
                'modal': true,
                'onshow': function(){
                    contentAddProgramWork.remove();
                },
                'onok': function(){
                    var form = $('#programWorkForm');


                    $.blockUI({message: '<h2>Подождите...</h2>'});

                    $.post("/workwithcoach/ajaxaddprogramm", form.serializeArray(),function( data ) {
                        $.unblockUI();
                        data = JSON.parse(data);
                        if(data.code == 1){
                            alertify.success(data.message);

                            setTimeout(function(){
                                location.reload();
                            }, 1000);
                            alertify.closeAll();
                        }
                        else{
                            alertify.error(data.error);
                        }
                    });
                },
                'oncancel': function(){

                }
            }).show();
    }

    body.on('click', '.addProgrammToWork', function(){
        addProgramWorkModal();
    });

    body.on('click', '.deleteProgramWork', function(){
        var id = $(this).data('work');
        $.post('/workwithcoach/ajaxadeleteprogramm', {id: id}, function(data){
            data = JSON.parse(data);
            if(data.code == 1){
                alertify.success(data.message);

                setTimeout(function(){
                    location.reload();
                }, 1000);
                alertify.closeAll();
            }
            else{
                alertify.error(data.error);
            }
        });

        return false;
    });

    body.on('click', '.programDelete', function(){
        var $this = $(this);
        var id = $this.data('id');
        var programBlock = $this.closest('.program_item_col');

        alertify.dialog('confirm')
            .set({
                'title' : 'Удалить программу тренировок',
                'labels':{ok:'Продолжить', cancel:'Отмена'},
                'message': 'Вы уверены что хотите удалить программу?' ,
                'modal': false,
                'pinnable': false,
                'onok': function(){
                    $.blockUI({message: '<h2>Подождите...</h2>'});
                    $.post('/profileprograms/ajaxdeleteprogram', {id: id}, function(data){
                        $.unblockUI();

                        if(data.response.code == 1){
                            programBlock.hide('fast', function(){ programBlock.remove();});
                            alertify.success(data.response.message);
                        }
                        else {
                            alertify.error(data.response.error);
                        }
                        alertify.closeAll();
                    });

                },
                'oncancel': function(){

                }
            }).show();
    });

    body.on('click', '.img_for_crop', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});
        var $img = '<div class="crop-image-block"><img height="500" class="crop_img" src="/uploads/users/original/'+$(this).data('photo')+'" /></div>';

        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Сохранить', cancel:'Отмена'},
                'message': $img ,
                'modal': false,
                'pinnable': false,
                onshow:function(){
                    $('.crop_img').cropper({
                        global: true,
                        dragCrop: true,
                        resizable: true,
                        aspectRatio: 1/1
                    });

                    $.unblockUI();
                },
                'onok': function(){
                    var cropData = $('.crop_img').cropper('getData');
                    $.post( "/profile/ajaxcropphoto", cropData, function(data){
                        data = $.parseJSON(data);
                        $('img.avatar_user_img').attr('src', data.domain + '/' + data.path + '/' + data.fileName);
                        $('img.avatar_user_img').data('photo', 'original_user_file_'+data.fileName);
                    });
                },
                'oncancel': function(){

                }
            }).unpin().show();
    });

    var contentProgress = $('.lk_user_progress_block').html();
    var initProgressValidate = false;
    var fileUploaded = false;

    body.on('click', '.editHistory', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});
        var progressId = $(this).data('id');
        fileUploaded = false;

        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Добавить', cancel:'Отмена'},
                'message': contentProgress ,
                'modal': false,
                'pinnable': true,
                onshow:function(){
                    $('.lk_user_progress_block').remove();
                    var form = $('#formProgress');

                    $('input:not(#ajax_type)', form).val('');
                    $('textarea', form).val('');

                    $.post( "/profile/ajaxgetprogressform", {id: progressId}, function(data){
                        var progress = data.response.progress;

                        $.each( progress, function( index, value ){
                            $('input[name="'+index+'"]', form).val(value);
                            $('textarea[name="'+index+'"]', form).val(value);
                        });

                        $('input#date', form).val($.datepicker.formatDate('dd.mm.yy', new Date(progress.date)));
                        $('input#thumbnail', form).val(progress.image);

                        var $img = '<img src="/uploads/ra/160x160/users/progress/original/' + progress.image+'" />';
                        $('.this-image .thumbnail_image', form).html($img);
                        fileUploaded = true;
                    });

                    resetValidateForm(form);
                    reInitDatapicker();
                    setupUpload();
                    $.unblockUI();
                },
                'onok': function(){
                    var form = $('#formProgress');

                    if(!initProgressValidate) {
                        form.validationEngine({
                            showPrompts: false,
                            scroll: false,
                            showOneMessage: true,
                            autoHidePrompt : true,
                            ajaxFormValidation: true,
                            onBeforeAjaxFormValidation: ajaxValidationProgressForm,
                            addSuccessCssClassToField : 'has-success',
                            addFailureCssClassToField : 'has-error',
                            onValidationComplete: function(form, status){
                                if(!status) {

                                }
                            }
                        });
                        initProgressValidate = true;
                    }

                    form.submit();

                    return false;
                },
                'oncancel': function(){

                }
            }).unpin().show();
    });

    var contentParams = $('.lk_user_params_block').html();
    var initParamsValidate = false;

    body.on('click', '.addParams', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});
        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Сохранить', cancel:'Отмена'},
                'message': contentParams ,
                'modal': true,
                'pinnable': true,
                onshow:function(){
                    $('.lk_user_params_block').remove();
                    var form = $('#paramsForm');

                    $('input', form).spinner({step: 0.1});

                    $('input', form).val('');
                    $('textarea', form).val('');

                    resetValidateForm(form);

                    $.post( "/profile/ajaxgetparams", {type: 'form'}, function(data){
                        var response = data.response;
                        $.each( response.form, function( index, value ){
                            $('input[name="'+index+'"]', form).val(value);
                        });
                    });
                    $.unblockUI();
                },
                'onok': function(){
                    var form = $('#paramsForm');

                    if(!initParamsValidate) {
                        form.validationEngine({
                            showPrompts: false,
                            scroll: false,
                            showOneMessage: true,
                            autoHidePrompt : true,
                            ajaxFormValidation: true,
                            onBeforeAjaxFormValidation: ajaxValidationParamsForm,
                            addSuccessCssClassToField : 'has-success',
                            addFailureCssClassToField : 'has-error',
                            onValidationComplete: function(form, status){
                                if(!status) {

                                }
                            }
                        });
                        initParamsValidate = true;
                    }

                    form.submit();

                    return false;
                },
                'oncancel': function(){

                }
            }).unpin().show();
    });

    var contentStrengh = $('.lk_user_strengh_block').html();
    var initStrenghValidate = false;

    body.on('click', '.addStrengh', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});
        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Сохранить', cancel:'Отмена'},
                'message': contentStrengh ,
                'modal': false,
                'pinnable': true,
                onshow:function(){
                    $('.lk_user_strengh_block').remove();
                    var form = $('#strenghForm');

                    $('input', form).spinner({step: 0.5});

                    $('input', form).val('');
                    $('textarea', form).val('');

                    resetValidateForm(form);

                    $.post( "/profile/ajaxgetstrengh", {type: 'form'}, function(data){
                        var response = data.response;
                        $.each( response.form, function( index, value ){
                            $('input[name="'+index+'"]', form).val(value);
                        });
                    });

                    $.unblockUI();
                },
                'onok': function(){
                    var form = $('#strenghForm');

                    if(!initStrenghValidate) {
                        form.validationEngine({
                            showPrompts: false,
                            scroll: false,
                            showOneMessage: true,
                            autoHidePrompt : true,
                            ajaxFormValidation: true,
                            onBeforeAjaxFormValidation: ajaxValidationStrenghForm,
                            addSuccessCssClassToField : 'has-success',
                            addFailureCssClassToField : 'has-error',
                            onValidationComplete: function(form, status){
                                if(!status) {

                                }
                            }
                        });
                        initStrenghValidate = true;
                    }

                    form.submit();

                    return false;
                },
                'oncancel': function(){

                }
            }).unpin().show();
    });

    body.on('click', '.addHistory', function(){
        $.blockUI({message: '<h2>Подождите...</h2>'});
        fileUploaded = false;
        alertify.dialog('confirm')
            .set({
                'labels':{ok:'Добавить', cancel:'Отмена'},
                'message': contentProgress ,
                'modal': false,
                'pinnable': true,
                onshow:function(){
                    $('.lk_user_progress_block').remove();

                    var form = $('#formProgress');
                    $('.thumbnail_image img', form).remove();

                    $('input:not(#ajax_type)', form).val('');
                    $('textarea', form).val('');

                    resetValidateForm(form);
                    reInitDatapicker();
                    setupUpload();

                    $.unblockUI();
                },
                'onok': function(){
                    var form = $('#formProgress');

                    if(!initProgressValidate) {
                        form.validationEngine({
                            showPrompts: false,
                            scroll: false,
                            showOneMessage: true,
                            autoHidePrompt : true,
                            ajaxFormValidation: true,
                            onBeforeAjaxFormValidation: ajaxValidationProgressForm,
                            addSuccessCssClassToField : 'has-success',
                            addFailureCssClassToField : 'has-error',
                            onValidationComplete: function(form, status){
                                if(!status) {

                                }
                            }
                        });
                        initProgressValidate = true;
                    }

                    form.submit();
                    return false;
                },
                'oncancel': function(){
                    var form = $('#formProgress');

                    var image = $('#thumbnail', form).val();
                    if(image != '') {
                        $.post( "/profile/ajaxdeletephoto", {image: image});
                    }
                }
            }).unpin().show();
    });

    function ajaxValidationProgressForm(form, options){
        var $data = form.serializeArray();
        $.blockUI({message: '<h2>Подождите...</h2>'});
        $('.ajax-lk-progress').html("");
        $('.lk-main-screen-progress-ajax').html("");
        loading("show", ".ajax-lk-progress");
        loading("show", ".lk-main-screen-progress-ajax");

        if(fileUploaded){
            $.ajax({
                url: "/profile/ajaxsaveprogress",
                data: $data,
                success: function (data) {
                    if (data.response.code == 0) {
                        alertify.error(data.response.error);
                        return false;
                    }
                    if (data.response.code == 1) {
                        alertify.closeAll();
                        alertify.success(data.response.message);

                        $.post( "/profile/ajaxgetprogress", {type: $('#ajax_type').val()}, function(data){
                            $('.ajax-lk-progress').html(data.response.html);
                            $('.lk-main-screen-progress-ajax').html(data.response.htmlshort);

                            loading("hide");
                        });
                    }

                    $.unblockUI();
                },
                cache: false, type: "POST", dataType: 'json'
            });
        }
        else {
            alertify.error('Добавьте фотографию');
        }

    }

    function ajaxValidationParamsForm(form, options){
        var $data = form.serializeArray();
        $.blockUI({message: '<h2>Подождите...</h2>'});

        $.ajax({
            url: "/profile/ajaxsaveparams",
            data: $data,
            success: function (data) {
                if (data.response.code == 0) {
                    alertify.error(data.response.error);
                    return false;
                }
                if (data.response.code == 1) {
                    alertify.closeAll();
                    alertify.success(data.response.message);

                    $.post( "/profile/ajaxgetparamshtml", function(data){
                        $('.ajax-lk-params').html(data.response.html);
                        $('.lk-main-screen-sizes-ajax').html(data.response.htmlshort);

                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }

                $.unblockUI();
            },
            cache: false, type: "POST", dataType: 'json'
        });
    }

    function ajaxValidationStrenghForm(form, options){
        var $data = form.serializeArray();

        $.ajax({
            url: "/profile/ajaxsavestrengh",
            data: $data,
            success: function (data) {
                if (data.response.code == 0) {
                    alertify.error(data.response.error);
                    return false;
                }
                if (data.response.code == 1) {
                    alertify.closeAll();
                    alertify.success(data.response.message);

                    $(".lk-main-screen-sizes-ajax").html(data.response.html);
                    $('[data-toggle="tooltip"]').tooltip();

                    /*
                    var bench = data.response.data.bench;
                    var lift = data.response.data.lift;
                    var squat = data.response.data.squat;

                    if(data.response.data.max_bench > 0) bench = data.response.data.max_bench;
                    if(data.response.data.max_lift > 0) lift = data.response.data.max_lift;
                    if(data.response.data.max_squat > 0) squat = data.response.data.max_squat;

                    $('.bench-params-value').text(bench);
                    $('.lift-params-value').text(lift);
                    $('.squat-params-value').text(squat);
                    */
                }
            },
            cache: false, type: "POST", dataType: 'json'
        });
    }

    function resetValidateForm(form){
        $('input', form).removeClass('has-error');
        $('textarea', form).removeClass('has-error');
        $('input', form).removeClass('has-success');
        $('textarea', form).removeClass('has-success');


    }

    function reInitDatapicker(){
        $('.datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd.mm.yy',
            maxDate: "+0M +0D"
        });
    }

    function ajaxChangePassword(form, options){
        var $data = form.serializeArray();

        $.ajax({
            url: "/profile/ajaxchangepassword",
            data: $data,
            success: function (data) {
                if (data.response.code == 0) {
                    alertify.error(data.response.error);
                    return false;
                }
                if (data.response.code == 1) {
                    alertify.success(data.response.message);
                    $('input', form).val('');

                    $('input', form).removeClass('has-error');
                    $('input', form).removeClass('has-success');
                }
            },
            cache: false, type: "POST", dataType: 'json'
        });

    }

    function setupUpload(){
        $('.file_upload').each(function() {
            var $this = $(this);
            var $file_one = [];
            $file_one['selector'] = $this;
            $file_one['script'] = $this.data('script');
            $file_one['single'] = $this.data('single');
            $file_one['type'] = $this.data('type');

            $this.fileupload({
                url: $file_one['script'],
                type: 'post',
                singleFileUploads: $file_one['single'],
                dataType: 'json',
                start: function (e, data) {
                    $.blockUI({message: '<h2>Подождите...</h2>'});
                },
                success: function (data) {
                    if($file_one['type'] == 'avatar') {
                        $('img.avatar_user_img').attr('src', data.domain + '/' + data.path + '/' + data.fileName);
                        $('img.avatar_user_img').data('photo', 'original_user_file_'+data.fileName);
                        if(!$('.lk_user_photo img.avatar_user_img').hasClass('img_for_crop'))
                            $('.lk_user_photo img.avatar_user_img').addClass('img_for_crop');
                    }
                    if($file_one['type'] == 'background') {
                        $('img.background_user_img').attr('src', data.domain + '/' + data.path + '/' + data.fileName);
                    }
                    if($file_one['type'] == 'progress') {
                        var $img = '<img src="'+data.domain + '/' + data.path + '/' + data.fileName+'" />';
                        $('.this-image .thumbnail_image').html($img);
                        $('#formProgress #thumbnail').val(data.fileName);
                        fileUploaded = true;
                    }
                    if($file_one['type'] == 'sertificate') {
                        $.each( data.fileNames, function( key, value ) {
                            var $img = '<div class="sertificate_image"><span class="deleteParent btn btn-danger fui-trash"></span><img src="'+data.domain + '/' + data.path + '/' + value+'" /><input type="hidden" name="sertificates[]" value="' + value+'"/></div>';
                            $('.sertificate_images').append($img);
                        });

                        fileUploaded = true;
                    }
                    if($file_one['type'] == 'workwithcoach') {
                        $.each( data.fileNames, function( key, value ) {
                            var $img = '<div class="workwithcoach_image"><span class="deleteParent btn btn-danger fui-trash"></span><img src="'+data.domain + '/' + data.path + '/' + value+'" /><input type="hidden" name="images[]" value="' + value+'"/></div>';
                            $('.workwithcoach_images').append($img);
                        });

                        fileUploaded = true;
                    }
                },
                done: function (e, data) {
                    $.unblockUI();
                },
                progressall: function (e, data) {
                    //var progress = parseInt(data.loaded / data.total * 100, 10);
                }
            });
        });
    }
});

jQuery.fn.OnlyNumbers =
    function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // Разрешаем backspace, tab, delete, стрелки, обычные цифры и цифры на дополнительной клавиатуре
                return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
            });
        });
    };
