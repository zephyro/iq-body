
jQuery(document).ready(function(){

    $('.rate-content').raty({
        number: 5,
        path    : 'img',
        score: function() {
            return $(this).attr('data-number');
        },
        readOnly: function() {
            return $(this).attr('data-readOnly');
        }
    });

});



// Header Menu

$(function() {
    var pull = $('.filter-toggle');
    var filter = $('.main-filter');

    $(pull).on('click', function(e) {
        e.preventDefault();
        filter.toggleClass('open');
    });
});


$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn_close" href="javascript:;"></a>'
    }
});


$('.btn-modal-close').on('click', function(e) {
    e.preventDefault();
    $('.btn_close').trigger('click');
});

// ----- Маска ----------
jQuery(function($){
    $("input[name='form_phone']").mask("+7(999) 999-9999");
});


$('.training-media-slider').slick({
    dots: true,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fui-arrow-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fui-arrow-right"></i></span>'
});


$('.filterTraining-title').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.filterTraining-box').toggleClass('filterTraining-toggle');
});


